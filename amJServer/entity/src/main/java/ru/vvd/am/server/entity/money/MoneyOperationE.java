package ru.vvd.am.server.entity.money;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "money_operation", schema = "public", catalog = "magic_academy_01")
public class MoneyOperationE {
    private int idOp;
    private Integer opsum;
    private Integer opdate;
    private String description;

    @Id
    @Column(name = "id_op", nullable = false)
    public int getIdOp() {
        return idOp;
    }

    public void setIdOp(int idOp) {
        this.idOp = idOp;
    }

    @Basic
    @Column(name = "opsum", nullable = true)
    public Integer getOpsum() {
        return opsum;
    }

    public void setOpsum(Integer opsum) {
        this.opsum = opsum;
    }

    @Basic
    @Column(name = "opdate", nullable = true)
    public Integer getOpdate() {
        return opdate;
    }

    public void setOpdate(Integer opdate) {
        this.opdate = opdate;
    }

    @Basic
    @Column(name = "description", nullable = true, length = -1)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoneyOperationE that = (MoneyOperationE) o;

        if (idOp != that.idOp) return false;
        if (opsum != null ? !opsum.equals(that.opsum) : that.opsum != null) return false;
        if (opdate != null ? !opdate.equals(that.opdate) : that.opdate != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idOp;
        result = 31 * result + (opsum != null ? opsum.hashCode() : 0);
        result = 31 * result + (opdate != null ? opdate.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
