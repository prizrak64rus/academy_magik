package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_dualities", schema = "public", catalog = "magic_academy_01")
public class CatalogDualitiesE {
    private int idDuality;
    private String name;
    private String antipodeName;

    @Id
    @Column(name = "id_duality", nullable = false)
    public int getIdDuality() {
        return idDuality;
    }

    public void setIdDuality(int idDuality) {
        this.idDuality = idDuality;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "antipode_name", nullable = true, length = 255)
    public String getAntipodeName() {
        return antipodeName;
    }

    public void setAntipodeName(String antipodeName) {
        this.antipodeName = antipodeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogDualitiesE that = (CatalogDualitiesE) o;

        if (idDuality != that.idDuality) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (antipodeName != null ? !antipodeName.equals(that.antipodeName) : that.antipodeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDuality;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (antipodeName != null ? antipodeName.hashCode() : 0);
        return result;
    }
}
