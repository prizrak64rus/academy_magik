package ru.vvd.am.server.entity.enums;

/**
 * Created by Prizrak on 02.05.2018.
 */
public enum ArtNotificationEnum {
    DIR("C:/arts"),
    DEFAULT("default.jpg"),
    TIME_OUT("timeOut.png")
    ;
    ArtNotificationEnum(String patch){
        this.patch=patch;
    }
    private String patch;

    public String getPatch() {
        return patch;
    }
}
