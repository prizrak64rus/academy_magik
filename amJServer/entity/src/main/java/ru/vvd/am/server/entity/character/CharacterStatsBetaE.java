package ru.vvd.am.server.entity.character;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "character_stats_beta", schema = "public", catalog = "magic_academy_01")
public class CharacterStatsBetaE {
    private int idCharacterStatsBeta;
    private int statValue;

    @Id
    @Column(name = "id_character_stats_beta", nullable = false)
    public int getIdCharacterStatsBeta() {
        return idCharacterStatsBeta;
    }

    public void setIdCharacterStatsBeta(int idCharacterStatsBeta) {
        this.idCharacterStatsBeta = idCharacterStatsBeta;
    }

    @Basic
    @Column(name = "stat_value", nullable = false)
    public int getStatValue() {
        return statValue;
    }

    public void setStatValue(int statValue) {
        this.statValue = statValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterStatsBetaE that = (CharacterStatsBetaE) o;

        if (idCharacterStatsBeta != that.idCharacterStatsBeta) return false;
        if (statValue != that.statValue) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCharacterStatsBeta;
        result = 31 * result + statValue;
        return result;
    }
}
