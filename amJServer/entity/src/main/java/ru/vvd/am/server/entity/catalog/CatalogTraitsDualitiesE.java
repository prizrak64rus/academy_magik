package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_traits_dualities", schema = "public", catalog = "magic_academy_01")
public class CatalogTraitsDualitiesE {
    private int idTraitDuality;
    private int value;

    @Id
    @Column(name = "id_trait_duality", nullable = false)
    public int getIdTraitDuality() {
        return idTraitDuality;
    }

    public void setIdTraitDuality(int idTraitDuality) {
        this.idTraitDuality = idTraitDuality;
    }

    @Basic
    @Column(name = "value", nullable = false)
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogTraitsDualitiesE that = (CatalogTraitsDualitiesE) o;

        if (idTraitDuality != that.idTraitDuality) return false;
        if (value != that.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTraitDuality;
        result = 31 * result + value;
        return result;
    }
}
