package ru.vvd.am.server.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Arrays;

/**
 * Created by Prizrak on 01.05.2018.
 */
@Entity
@Table(name = "help_data", schema = "public", catalog = "magic_academy_01")
public class HelpDataE {
    private Integer idHelp;
    private byte[] anonimArt;
    private Date lastUpdateArt;

    @Id
    @Column(name = "id_help", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdHelp() {
        return idHelp;
    }

    public void setIdHelp(Integer idHelp) {
        this.idHelp = idHelp;
    }

    @Basic
    @Column(name = "anonim_art", nullable = false)
    public byte[] getAnonimArt() {
        return anonimArt;
    }

    public void setAnonimArt(byte[] anonimArt) {
        this.anonimArt = anonimArt;
    }

    @Basic
    @Column(name = "last_update_art", nullable = false)
    public Date getLastUpdateArt() {
        return lastUpdateArt;
    }

    public void setLastUpdateArt(Date lastUpdateArt) {
        this.lastUpdateArt = lastUpdateArt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HelpDataE helpDataE = (HelpDataE) o;

        if (idHelp != helpDataE.idHelp) return false;
        if (!Arrays.equals(anonimArt, helpDataE.anonimArt)) return false;
        if (lastUpdateArt != null ? !lastUpdateArt.equals(helpDataE.lastUpdateArt) : helpDataE.lastUpdateArt != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idHelp != null ? idHelp : 0;
        result = 31 * result + Arrays.hashCode(anonimArt);
        result = 31 * result + (lastUpdateArt != null ? lastUpdateArt.hashCode() : 0);
        return result;
    }
}
