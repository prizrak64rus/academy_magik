package ru.vvd.am.server.utils;

import ru.vvd.am.server.entity.magician.player.Messages;

import java.util.*;

/**
 * Created by Prizrak on 11.01.2018.
 */
public class ChatData {

    private static ChatData chatData;
    private static Map<String,ArrayList<Messages>> messagesMap = new HashMap<String, ArrayList<Messages>>();
    private final Object lock = new Object();

    private ChatData() {
    }

    public static ChatData getChatData(){
        if(chatData==null){
            chatData=new ChatData();
        }
        return chatData;
    }

    public void writeMessages(Messages message){
        synchronized (lock)        {
            ArrayList<Messages> messages = messagesMap.get(message.getToWhom());
            if(messages==null){
                messages = new ArrayList<Messages>();
                messagesMap.put(message.getToWhom(),messages);
            }
            messages.add(message);
        }
    }

    public List<Messages> checkMessages(String str){
        synchronized (lock)        {
            ArrayList<Messages> messages = messagesMap.get(str);
            if(messages==null){
                return Collections.EMPTY_LIST;
            }
            ArrayList<Messages> messagesSend = new ArrayList<Messages>(messages);
            messages.clear();
            return messagesSend;
        }
    }



}
