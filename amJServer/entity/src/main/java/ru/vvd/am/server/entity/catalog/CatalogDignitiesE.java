package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_dignities", schema = "public", catalog = "magic_academy_01")
public class CatalogDignitiesE {
    private int idDignity;
    private String dignity;
    private int rankAmmount;

    @Id
    @Column(name = "id_dignity", nullable = false)
    public int getIdDignity() {
        return idDignity;
    }

    public void setIdDignity(int idDignity) {
        this.idDignity = idDignity;
    }

    @Basic
    @Column(name = "dignity", nullable = false, length = 255)
    public String getDignity() {
        return dignity;
    }

    public void setDignity(String dignity) {
        this.dignity = dignity;
    }

    @Basic
    @Column(name = "rank_ammount", nullable = false)
    public int getRankAmmount() {
        return rankAmmount;
    }

    public void setRankAmmount(int rankAmmount) {
        this.rankAmmount = rankAmmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogDignitiesE that = (CatalogDignitiesE) o;

        if (idDignity != that.idDignity) return false;
        if (rankAmmount != that.rankAmmount) return false;
        if (dignity != null ? !dignity.equals(that.dignity) : that.dignity != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDignity;
        result = 31 * result + (dignity != null ? dignity.hashCode() : 0);
        result = 31 * result + rankAmmount;
        return result;
    }
}
