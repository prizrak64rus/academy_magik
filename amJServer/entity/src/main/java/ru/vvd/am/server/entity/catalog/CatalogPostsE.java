package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_posts", schema = "public", catalog = "magic_academy_01")
public class CatalogPostsE {
    private int idPost;
    private String post;

    @Id
    @Column(name = "id_post", nullable = false)
    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    @Basic
    @Column(name = "post", nullable = false, length = 255)
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogPostsE that = (CatalogPostsE) o;

        if (idPost != that.idPost) return false;
        if (post != null ? !post.equals(that.post) : that.post != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPost;
        result = 31 * result + (post != null ? post.hashCode() : 0);
        return result;
    }
}
