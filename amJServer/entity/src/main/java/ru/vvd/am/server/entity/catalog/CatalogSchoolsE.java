package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_schools", schema = "public", catalog = "magic_academy_01")
public class CatalogSchoolsE {
    private int idSchool;
    private String name;
    private short rowNum;

    @Id
    @Column(name = "id_school", nullable = false)
    public int getIdSchool() {
        return idSchool;
    }

    public void setIdSchool(int idSchool) {
        this.idSchool = idSchool;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "row_num", nullable = false)
    public short getRowNum() {
        return rowNum;
    }

    public void setRowNum(short rowNum) {
        this.rowNum = rowNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogSchoolsE that = (CatalogSchoolsE) o;

        if (idSchool != that.idSchool) return false;
        if (rowNum != that.rowNum) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSchool;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) rowNum;
        return result;
    }
}
