package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy_student_progress", schema = "public", catalog = "magic_academy_01")
public class AcademyStudentProgressE {
    private int idStudentProgress;
    private Integer pageAmmount;
    private Boolean isFinished;

    @Id
    @Column(name = "id_student_progress", nullable = false)
    public int getIdStudentProgress() {
        return idStudentProgress;
    }

    public void setIdStudentProgress(int idStudentProgress) {
        this.idStudentProgress = idStudentProgress;
    }

    @Basic
    @Column(name = "page_ammount", nullable = true)
    public Integer getPageAmmount() {
        return pageAmmount;
    }

    public void setPageAmmount(Integer pageAmmount) {
        this.pageAmmount = pageAmmount;
    }

    @Basic
    @Column(name = "is_finished", nullable = true)
    public Boolean getFinished() {
        return isFinished;
    }

    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyStudentProgressE that = (AcademyStudentProgressE) o;

        if (idStudentProgress != that.idStudentProgress) return false;
        if (pageAmmount != null ? !pageAmmount.equals(that.pageAmmount) : that.pageAmmount != null) return false;
        if (isFinished != null ? !isFinished.equals(that.isFinished) : that.isFinished != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudentProgress;
        result = 31 * result + (pageAmmount != null ? pageAmmount.hashCode() : 0);
        result = 31 * result + (isFinished != null ? isFinished.hashCode() : 0);
        return result;
    }
}
