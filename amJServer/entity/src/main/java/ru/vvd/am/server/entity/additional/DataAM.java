package ru.vvd.am.server.entity.additional;

import ru.vvd.am.server.entity.enums.TypeDataAMEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class DataAM {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private TypeDataAMEnum dataAMEnum;
    private Integer dataAM;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TypeDataAMEnum getDataAMEnum() {
        return dataAMEnum;
    }

    public void setDataAMEnum(TypeDataAMEnum dataAMEnum) {
        this.dataAMEnum = dataAMEnum;
    }

    public Integer getDataAM() {
        return dataAM;
    }

    public void setDataAM(Integer dataAM) {
        this.dataAM = dataAM;
    }
}
