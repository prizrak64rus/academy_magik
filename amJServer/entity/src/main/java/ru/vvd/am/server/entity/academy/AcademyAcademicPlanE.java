package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy_academic_plan", schema = "public", catalog = "magic_academy_01")
public class AcademyAcademicPlanE {
    private int idAcademicPlan;
    private int course;
    private int pageAmmountS1;
    private int pageAmmountS2;
    private int pageAmmountS3;
    private int pageAmmountS4;

    @Id
    @Column(name = "id_academic_plan", nullable = false)
    public int getIdAcademicPlan() {
        return idAcademicPlan;
    }

    public void setIdAcademicPlan(int idAcademicPlan) {
        this.idAcademicPlan = idAcademicPlan;
    }

    @Basic
    @Column(name = "course", nullable = false)
    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Basic
    @Column(name = "page_ammount_s1", nullable = false)
    public int getPageAmmountS1() {
        return pageAmmountS1;
    }

    public void setPageAmmountS1(int pageAmmountS1) {
        this.pageAmmountS1 = pageAmmountS1;
    }

    @Basic
    @Column(name = "page_ammount_s2", nullable = false)
    public int getPageAmmountS2() {
        return pageAmmountS2;
    }

    public void setPageAmmountS2(int pageAmmountS2) {
        this.pageAmmountS2 = pageAmmountS2;
    }

    @Basic
    @Column(name = "page_ammount_s3", nullable = false)
    public int getPageAmmountS3() {
        return pageAmmountS3;
    }

    public void setPageAmmountS3(int pageAmmountS3) {
        this.pageAmmountS3 = pageAmmountS3;
    }

    @Basic
    @Column(name = "page_ammount_s4", nullable = false)
    public int getPageAmmountS4() {
        return pageAmmountS4;
    }

    public void setPageAmmountS4(int pageAmmountS4) {
        this.pageAmmountS4 = pageAmmountS4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyAcademicPlanE that = (AcademyAcademicPlanE) o;

        if (idAcademicPlan != that.idAcademicPlan) return false;
        if (course != that.course) return false;
        if (pageAmmountS1 != that.pageAmmountS1) return false;
        if (pageAmmountS2 != that.pageAmmountS2) return false;
        if (pageAmmountS3 != that.pageAmmountS3) return false;
        if (pageAmmountS4 != that.pageAmmountS4) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAcademicPlan;
        result = 31 * result + course;
        result = 31 * result + pageAmmountS1;
        result = 31 * result + pageAmmountS2;
        result = 31 * result + pageAmmountS3;
        result = 31 * result + pageAmmountS4;
        return result;
    }
}
