package ru.vvd.am.server.entity.magician;

import ru.vvd.am.server.entity.magician.player.MoneyOperationHistory;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class Сlub {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String nameClub;
    private Integer kourse1;
    private Integer kourse2;
    private Integer kourse3;
    private Integer percent;
    private Integer top10;
    private Integer top100;
    private Integer salary;
    private Integer money;
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="club_id")
    private Set<MoneyOperationHistory> moneyOperationHistories;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameClub() {
        return nameClub;
    }

    public void setNameClub(String nameClub) {
        this.nameClub = nameClub;
    }

    public Integer getKourse1() {
        return kourse1;
    }

    public void setKourse1(Integer kourse1) {
        this.kourse1 = kourse1;
    }

    public Integer getKourse2() {
        return kourse2;
    }

    public void setKourse2(Integer kourse2) {
        this.kourse2 = kourse2;
    }

    public Integer getKourse3() {
        return kourse3;
    }

    public void setKourse3(Integer kourse3) {
        this.kourse3 = kourse3;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public Integer getTop10() {
        return top10;
    }

    public void setTop10(Integer top10) {
        this.top10 = top10;
    }

    public Integer getTop100() {
        return top100;
    }

    public void setTop100(Integer top100) {
        this.top100 = top100;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Set<MoneyOperationHistory> getMoneyOperationHistories() {
        return moneyOperationHistories;
    }

    public void setMoneyOperationHistories(Set<MoneyOperationHistory> moneyOperationHistories) {
        this.moneyOperationHistories = moneyOperationHistories;
    }
}
