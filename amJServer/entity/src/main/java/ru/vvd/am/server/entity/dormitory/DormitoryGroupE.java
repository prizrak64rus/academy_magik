package ru.vvd.am.server.entity.dormitory;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "dormitory_group", schema = "public", catalog = "magic_academy_01")
public class DormitoryGroupE {
    private int idDormitoryGroup;
    private int number;

    @Id
    @Column(name = "id_dormitory_group", nullable = false)
    public int getIdDormitoryGroup() {
        return idDormitoryGroup;
    }

    public void setIdDormitoryGroup(int idDormitoryGroup) {
        this.idDormitoryGroup = idDormitoryGroup;
    }

    @Basic
    @Column(name = "number", nullable = false)
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DormitoryGroupE that = (DormitoryGroupE) o;

        if (idDormitoryGroup != that.idDormitoryGroup) return false;
        if (number != that.number) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDormitoryGroup;
        result = 31 * result + number;
        return result;
    }
}
