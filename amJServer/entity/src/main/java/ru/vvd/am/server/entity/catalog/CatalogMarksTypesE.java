package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_marks_types", schema = "public", catalog = "magic_academy_01")
public class CatalogMarksTypesE {
    private int idMarkType;
    private String name;

    @Id
    @Column(name = "id_mark_type", nullable = false)
    public int getIdMarkType() {
        return idMarkType;
    }

    public void setIdMarkType(int idMarkType) {
        this.idMarkType = idMarkType;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogMarksTypesE that = (CatalogMarksTypesE) o;

        if (idMarkType != that.idMarkType) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMarkType;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
