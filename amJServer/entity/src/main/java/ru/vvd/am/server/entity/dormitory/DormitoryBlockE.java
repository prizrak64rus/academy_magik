package ru.vvd.am.server.entity.dormitory;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "dormitory_block", schema = "public", catalog = "magic_academy_01")
public class DormitoryBlockE {
    private int idDormitoryBlock;
    private Integer block;

    @Id
    @Column(name = "id_dormitory_block", nullable = false)
    public int getIdDormitoryBlock() {
        return idDormitoryBlock;
    }

    public void setIdDormitoryBlock(int idDormitoryBlock) {
        this.idDormitoryBlock = idDormitoryBlock;
    }

    @Basic
    @Column(name = "block", nullable = true)
    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DormitoryBlockE that = (DormitoryBlockE) o;

        if (idDormitoryBlock != that.idDormitoryBlock) return false;
        if (block != null ? !block.equals(that.block) : that.block != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDormitoryBlock;
        result = 31 * result + (block != null ? block.hashCode() : 0);
        return result;
    }
}
