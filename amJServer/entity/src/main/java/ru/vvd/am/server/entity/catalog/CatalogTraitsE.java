package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_traits", schema = "public", catalog = "magic_academy_01")
public class CatalogTraitsE {
    private int idTrait;
    private String name;

    @Id
    @Column(name = "id_trait", nullable = false)
    public int getIdTrait() {
        return idTrait;
    }

    public void setIdTrait(int idTrait) {
        this.idTrait = idTrait;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogTraitsE that = (CatalogTraitsE) o;

        if (idTrait != that.idTrait) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTrait;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
