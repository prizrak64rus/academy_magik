package ru.vvd.am.server.utils;

import ru.vvd.am.server.entity.ArtNotification;

import java.util.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
public class ArtData {
    private static ArtData artData;
    private static ArrayList<ArtNotification> artNotifications = new ArrayList<ArtNotification>();
    private final Object lock = new Object();

    private ArtData() {
    }

    public static ArtData getArtData(){
        if(artData==null){
            artData=new ArtData();
        }
        return artData;
    }

    public void writeArtNotification(ArtNotification notification){
        synchronized (lock)        {
            artNotifications.remove(notification);
            artNotifications.add(notification);
        }
    }

    public List<ArtNotification> checkArtNotification(){
        synchronized (lock)        {
            return new ArrayList<ArtNotification>(artNotifications);
        }
    }
}
