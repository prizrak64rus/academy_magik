package ru.vvd.am.server.entity.dormitory;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "dormitory", schema = "public", catalog = "magic_academy_01")
public class DormitoryE {
    private int idDormitory;
    private String dormitory;

    @Id
    @Column(name = "id_dormitory", nullable = false)
    public int getIdDormitory() {
        return idDormitory;
    }

    public void setIdDormitory(int idDormitory) {
        this.idDormitory = idDormitory;
    }

    @Basic
    @Column(name = "dormitory", nullable = true, length = 255)
    public String getDormitory() {
        return dormitory;
    }

    public void setDormitory(String dormitory) {
        this.dormitory = dormitory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DormitoryE that = (DormitoryE) o;

        if (idDormitory != that.idDormitory) return false;
        if (dormitory != null ? !dormitory.equals(that.dormitory) : that.dormitory != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDormitory;
        result = 31 * result + (dormitory != null ? dormitory.hashCode() : 0);
        return result;
    }
}
