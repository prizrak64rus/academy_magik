package ru.vvd.am.server.entity.money;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "money_optytes", schema = "public", catalog = "magic_academy_01")
public class MoneyOptytesE {
    private int idTop;
    private String name;

    @Id
    @Column(name = "id_top", nullable = false)
    public int getIdTop() {
        return idTop;
    }

    public void setIdTop(int idTop) {
        this.idTop = idTop;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoneyOptytesE that = (MoneyOptytesE) o;

        if (idTop != that.idTop) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTop;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
