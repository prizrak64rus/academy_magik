package ru.vvd.am.server.entity.character;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "character", schema = "public", catalog = "magic_academy_01")
public class CharacterE {
    private Integer idCharacter;
    private boolean isNpc;

    @Id
    @Column(name = "id_character", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(Integer idCharacter) {
        this.idCharacter = idCharacter;
    }

    @Basic
    @Column(name = "is_npc", nullable = false)
    public boolean isNpc() {
        return isNpc;
    }

    public void setNpc(boolean npc) {
        isNpc = npc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterE that = (CharacterE) o;

        if (idCharacter != that.idCharacter) return false;
        if (isNpc != that.isNpc) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCharacter;
        result = 31 * result + (isNpc ? 1 : 0);
        return result;
    }
}
