package ru.vvd.am.server.entity.personality;

import ru.vvd.am.server.entity.enums.SexEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "personality_data", schema = "public", catalog = "magic_academy_01")
public class PersonalityDataE {
    private Integer idPersonalityData;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String alias;
    private SexEnum sex;
    private Integer age;
    private String placeOfBirth;
    private Integer psy;
    private String club;
    private String post;

    @Id
    @Column(name = "id_personality_data", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdPersonalityData() {
        return idPersonalityData;
    }

    public void setIdPersonalityData(Integer idPersonalityData) {
        this.idPersonalityData = idPersonalityData;
    }

    @Basic
    @Column(name = "first_name", nullable = true, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "second_name", nullable = true, length = 255)
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Basic
    @Column(name = "last_name", nullable = true, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "short_name", nullable = true, length = 255)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Basic
    @Column(name = "alias", nullable = true, length = 255)
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Basic
    @Column(name = "sex", nullable = false)
    @Enumerated(EnumType.STRING)
    public SexEnum isSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "age", nullable = true)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Basic
    @Column(name = "place_of_birth", nullable = true, length = 255)
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    @Basic
    @Column(name = "psy", nullable = true)
    public Integer getPsy() {
        return psy;
    }

    public void setPsy(Integer psy) {
        this.psy = psy;
    }

    @Basic
    @Column(name = "club", nullable = true, length = 255)
    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    @Basic
    @Column(name = "post", nullable = true, length = 255)
    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonalityDataE that = (PersonalityDataE) o;

        if (idPersonalityData != that.idPersonalityData) return false;
        if (sex != that.sex) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (secondName != null ? !secondName.equals(that.secondName) : that.secondName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (shortName != null ? !shortName.equals(that.shortName) : that.shortName != null) return false;
        if (alias != null ? !alias.equals(that.alias) : that.alias != null) return false;
        if (age != null ? !age.equals(that.age) : that.age != null) return false;
        if (placeOfBirth != null ? !placeOfBirth.equals(that.placeOfBirth) : that.placeOfBirth != null) return false;
        if (psy != null ? !psy.equals(that.psy) : that.psy != null) return false;
        if (club != null ? !club.equals(that.club) : that.club != null) return false;
        if (post != null ? !post.equals(that.post) : that.post != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPersonalityData;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        result = 31 * result + (sex.getVal() ? 1 : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (placeOfBirth != null ? placeOfBirth.hashCode() : 0);
        result = 31 * result + (psy != null ? psy.hashCode() : 0);
        result = 31 * result + (club != null ? club.hashCode() : 0);
        result = 31 * result + (post != null ? post.hashCode() : 0);
        return result;
    }
}
