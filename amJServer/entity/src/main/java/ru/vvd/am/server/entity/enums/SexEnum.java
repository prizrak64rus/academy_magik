package ru.vvd.am.server.entity.enums;

/**
 * Created by Prizrak on 08.01.2018.
 */
public enum SexEnum {
    FEMALE(false),
    MALE(true);
    boolean val;
    SexEnum(boolean val){
        this.val=val;
    }
    public boolean getVal(){
        return val;
    }
}
