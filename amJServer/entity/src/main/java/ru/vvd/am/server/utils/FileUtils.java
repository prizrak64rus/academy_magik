package ru.vvd.am.server.utils;

import ru.vvd.am.server.entity.enums.ArtNotificationEnum;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Prizrak on 02.05.2018.
 */
public class FileUtils {
    public static final String DELIMETER = "/";
    public static final String TIME_OUT="timeOut";

    public static byte[] getByteArrayFromFilePatch(String patch) {
        try {
            File file = new File(patch);
            byte[] bytes = new byte[(int) file.length()];
            FileInputStream is = new FileInputStream(file);
            is.read(bytes);
            return bytes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
        // OutputStream os = new ByteArrayOutputStream(f);
    }

    public static Map<String, byte[]> fillingAvatarsMap(List<String> names) {
        Map avatarsMap = new HashMap<String, byte[]>();
        byte[] art = getByteArrayFromFilePatch(ArtNotificationEnum.DIR.getPatch()
                + DELIMETER + ArtNotificationEnum.TIME_OUT.getPatch());
        avatarsMap.put(TIME_OUT, art);
        for (String namne : names) {
            art = getByteArrayFromFilePatch(ArtNotificationEnum.DIR.getPatch()
                    + DELIMETER + namne + FileUtils.DELIMETER + ArtNotificationEnum.DEFAULT.getPatch());
            avatarsMap.put(namne, art);
        }
        return avatarsMap;
    }
}
