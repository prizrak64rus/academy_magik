package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_stats", schema = "public", catalog = "magic_academy_01")
public class CatalogStatsE {
    private int idStat;
    private String name;
    private int minValue;
    private int maxValue;

    @Id
    @Column(name = "id_stat", nullable = false)
    public int getIdStat() {
        return idStat;
    }

    public void setIdStat(int idStat) {
        this.idStat = idStat;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "min_value", nullable = false)
    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    @Basic
    @Column(name = "max_value", nullable = false)
    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogStatsE that = (CatalogStatsE) o;

        if (idStat != that.idStat) return false;
        if (minValue != that.minValue) return false;
        if (maxValue != that.maxValue) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStat;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + minValue;
        result = 31 * result + maxValue;
        return result;
    }
}
