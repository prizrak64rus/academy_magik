package ru.vvd.am.server.entity.magician.player;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class Messages {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String fromWhom;
    private Integer date;
    private String message;
    private String toWhom;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromWhom() {
        return fromWhom;
    }

    public void setFromWhom(String fromWhom) {
        this.fromWhom = fromWhom;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToWhom() {
        return toWhom;
    }

    public void setToWhom(String toWhom) {
        this.toWhom = toWhom;
    }

    public Messages(String fromWhom, Integer date, String message, String toWhom) {
        this.fromWhom = fromWhom;
        this.date = date;
        this.message = message;
        this.toWhom = toWhom;
    }

    public Messages() {
    }
}
