package ru.vvd.am.server.entity;

/**
 * Created by Prizrak on 30.04.2018.
 */
public class ArtNotification {
    private String who;
    private String charaster;

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getCharaster() {
        return charaster;
    }

    public void setCharaster(String charaster) {
        this.charaster = charaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArtNotification that = (ArtNotification) o;

        return who != null ? who.equals(that.who) : that.who == null;
    }

    @Override
    public int hashCode() {
        return who != null ? who.hashCode() : 0;
    }
}
