package ru.vvd.am.server.entity;

import ru.vvd.am.server.entity.personality.PersonalityE;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "user", schema = "public", catalog = "magic_academy_01")
public class UserE {
    private Integer idUser;
    private String name;
    private String password;
    private PersonalityE personality;
    private HelpDataE helpData;
    private List<String> userList;
    private Map<String, byte[]> avatarsMap;

    @Id
    @Column(name = "id_user", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 50)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="id_personality", referencedColumnName="id_personality")
    public PersonalityE getPersonality() {
        return personality;
    }

    public void setPersonality(PersonalityE personality) {
        this.personality = personality;
    }

    @Transient
    public HelpDataE getHelpData() {
        return helpData;
    }

    public void setHelpData(HelpDataE helpData) {
        this.helpData = helpData;
    }

    @Transient
    public List<String> getUserList() {
        return userList;
    }

    public void setUserList(List<String> userList) {
        this.userList = userList;
    }

    @Transient
    public Map<String, byte[]> getAvatarsMap() {
        return avatarsMap;
    }

    public void setAvatarsMap(Map<String, byte[]> avatarsMap) {
        this.avatarsMap = avatarsMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserE userE = (UserE) o;

        if (idUser != userE.idUser) return false;
        if (name != null ? !name.equals(userE.name) : userE.name != null) return false;
        if (password != null ? !password.equals(userE.password) : userE.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (idUser != null ? idUser.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
