package ru.vvd.am.server.entity.additional;

import ru.vvd.am.server.entity.enums.SexEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class Names {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;
    @Enumerated(EnumType.STRING)
    private SexEnum sex;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }
}
