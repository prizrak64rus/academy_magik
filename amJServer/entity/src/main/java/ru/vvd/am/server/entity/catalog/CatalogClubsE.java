package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_clubs", schema = "public", catalog = "magic_academy_01")
public class CatalogClubsE {
    private int idClub;
    private String club;

    @Id
    @Column(name = "id_club", nullable = false)
    public int getIdClub() {
        return idClub;
    }

    public void setIdClub(int idClub) {
        this.idClub = idClub;
    }

    @Basic
    @Column(name = "club", nullable = false, length = 255)
    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogClubsE that = (CatalogClubsE) o;

        if (idClub != that.idClub) return false;
        if (club != null ? !club.equals(that.club) : that.club != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idClub;
        result = 31 * result + (club != null ? club.hashCode() : 0);
        return result;
    }
}
