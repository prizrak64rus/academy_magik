package ru.vvd.am.server.entity.personality;

import ru.vvd.am.server.entity.character.CharacterE;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "personality", schema = "public", catalog = "magic_academy_01")
public class PersonalityE {
    private Integer idPersonality;
    private Boolean isActive;
    private PersonalityDataE personalityData;
    private CharacterE character;
    private boolean isArtExist;

    @Id
    @Column(name = "id_personality", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdPersonality() {
        return idPersonality;
    }

    public void setIdPersonality(Integer idPersonality) {
        this.idPersonality = idPersonality;
    }

    @Basic
    @Column(name = "is_active", nullable = true)
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="id_personality_data", referencedColumnName="id_personality_data")
    public PersonalityDataE getPersonalityData() {
        return personalityData;
    }

    public void setPersonalityData(PersonalityDataE personalityData) {
        this.personalityData = personalityData;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="id_character", referencedColumnName="id_character")
    public CharacterE getCharacter() {
        return character;
    }

    public void setCharacter(CharacterE character) {
        this.character = character;
    }

    @Basic
    @Column(name = "is_art_exist", nullable = false)
    public boolean isArtExist() {
        return isArtExist;
    }

    public void setArtExist(boolean artExist) {
        isArtExist = artExist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonalityE that = (PersonalityE) o;

        if (idPersonality != that.idPersonality) return false;
        if (isActive != null ? !isActive.equals(that.isActive) : that.isActive != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPersonality;
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        return result;
    }
}
