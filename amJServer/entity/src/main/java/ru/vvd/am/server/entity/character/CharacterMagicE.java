package ru.vvd.am.server.entity.character;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "character_magic", schema = "public", catalog = "magic_academy_01")
public class CharacterMagicE {
    private int idPersonalityMagic;
    private Integer value;

    @Id
    @Column(name = "id_personality_magic", nullable = false)
    public int getIdPersonalityMagic() {
        return idPersonalityMagic;
    }

    public void setIdPersonalityMagic(int idPersonalityMagic) {
        this.idPersonalityMagic = idPersonalityMagic;
    }

    @Basic
    @Column(name = "value", nullable = true)
    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterMagicE that = (CharacterMagicE) o;

        if (idPersonalityMagic != that.idPersonalityMagic) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPersonalityMagic;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
