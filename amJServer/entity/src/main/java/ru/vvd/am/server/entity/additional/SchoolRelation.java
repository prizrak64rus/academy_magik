package ru.vvd.am.server.entity.additional;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class SchoolRelation {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="school_id", referencedColumnName="id")
    private SchoolChance school;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="school_other_id", referencedColumnName="id")
    private SchoolChance schoolOther;
    private Integer percent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SchoolChance getSchool() {
        return school;
    }

    public void setSchool(SchoolChance school) {
        this.school = school;
    }

    public SchoolChance getSchoolOther() {
        return schoolOther;
    }

    public void setSchoolOther(SchoolChance schoolOther) {
        this.schoolOther = schoolOther;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }
}
