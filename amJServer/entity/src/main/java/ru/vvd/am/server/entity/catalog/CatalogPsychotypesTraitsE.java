package ru.vvd.am.server.entity.catalog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_psychotypes_traits", schema = "public", catalog = "magic_academy_01")
public class CatalogPsychotypesTraitsE {
    private int idPsychotypesTraits;

    @Id
    @Column(name = "id_psychotypes_traits", nullable = false)
    public int getIdPsychotypesTraits() {
        return idPsychotypesTraits;
    }

    public void setIdPsychotypesTraits(int idPsychotypesTraits) {
        this.idPsychotypesTraits = idPsychotypesTraits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogPsychotypesTraitsE that = (CatalogPsychotypesTraitsE) o;

        if (idPsychotypesTraits != that.idPsychotypesTraits) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return idPsychotypesTraits;
    }
}
