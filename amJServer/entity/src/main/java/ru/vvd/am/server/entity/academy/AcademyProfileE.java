package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy_profile", schema = "public", catalog = "magic_academy_01")
public class AcademyProfileE {
    private int idProfile;
    private String personalNumber;
    private Integer course;
    private Integer flow;
    private Integer group;
    private Integer block;
    private Integer room;
    private Integer cockpit;
    private int dateStart;
    private Integer dateEnd;

    @Id
    @Column(name = "id_profile", nullable = false)
    public int getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(int idProfile) {
        this.idProfile = idProfile;
    }

    @Basic
    @Column(name = "personal_number", nullable = true, length = 255)
    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    @Basic
    @Column(name = "course", nullable = true)
    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    @Basic
    @Column(name = "flow", nullable = true)
    public Integer getFlow() {
        return flow;
    }

    public void setFlow(Integer flow) {
        this.flow = flow;
    }

    @Basic
    @Column(name = "group", nullable = true)
    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    @Basic
    @Column(name = "block", nullable = true)
    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    @Basic
    @Column(name = "room", nullable = true)
    public Integer getRoom() {
        return room;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    @Basic
    @Column(name = "cockpit", nullable = true)
    public Integer getCockpit() {
        return cockpit;
    }

    public void setCockpit(Integer cockpit) {
        this.cockpit = cockpit;
    }

    @Basic
    @Column(name = "date_start", nullable = false)
    public int getDateStart() {
        return dateStart;
    }

    public void setDateStart(int dateStart) {
        this.dateStart = dateStart;
    }

    @Basic
    @Column(name = "date_end", nullable = true)
    public Integer getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Integer dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyProfileE that = (AcademyProfileE) o;

        if (idProfile != that.idProfile) return false;
        if (dateStart != that.dateStart) return false;
        if (personalNumber != null ? !personalNumber.equals(that.personalNumber) : that.personalNumber != null)
            return false;
        if (course != null ? !course.equals(that.course) : that.course != null) return false;
        if (flow != null ? !flow.equals(that.flow) : that.flow != null) return false;
        if (group != null ? !group.equals(that.group) : that.group != null) return false;
        if (block != null ? !block.equals(that.block) : that.block != null) return false;
        if (room != null ? !room.equals(that.room) : that.room != null) return false;
        if (cockpit != null ? !cockpit.equals(that.cockpit) : that.cockpit != null) return false;
        if (dateEnd != null ? !dateEnd.equals(that.dateEnd) : that.dateEnd != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idProfile;
        result = 31 * result + (personalNumber != null ? personalNumber.hashCode() : 0);
        result = 31 * result + (course != null ? course.hashCode() : 0);
        result = 31 * result + (flow != null ? flow.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (block != null ? block.hashCode() : 0);
        result = 31 * result + (room != null ? room.hashCode() : 0);
        result = 31 * result + (cockpit != null ? cockpit.hashCode() : 0);
        result = 31 * result + dateStart;
        result = 31 * result + (dateEnd != null ? dateEnd.hashCode() : 0);
        return result;
    }
}
