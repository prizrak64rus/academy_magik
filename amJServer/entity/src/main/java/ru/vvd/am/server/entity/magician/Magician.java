package ru.vvd.am.server.entity.magician;

import ru.vvd.am.server.entity.enums.SexEnum;
import ru.vvd.am.server.entity.enums.StatusOfMagicianEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@MappedSuperclass
public class Magician {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;
    @Enumerated(EnumType.STRING)
    private SexEnum sex;
    private Integer course;
    private Integer room;
    private Integer block;
    private Integer groupNumber;
    private Integer flow;
    private Integer cod;
    private String clubBrotherhood;
    private String magikType;
    private String magik;
    private String sopMagik;
    private Integer str;
    private Integer agil;
    private Integer konst;
    private Integer stamin;
    private Integer speed;
    private Integer intel;
    private Integer wisp;
    private Integer will;
    private Integer exp;
    private Integer charm;
    private Integer title;
    private Integer rank;
    @Enumerated(EnumType.STRING)
    private StatusOfMagicianEnum status;
    private Integer psy;
    private Integer money;

    public Integer getTitle() {
        return title;
    }

    public void setTitle(Integer title) {
        this.title = title;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getRoom() {
        return room;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public Integer getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Integer groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Integer getFlow() {
        return flow;
    }

    public void setFlow(Integer flow) {
        this.flow = flow;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public String getClubBrotherhood() {
        return clubBrotherhood;
    }

    public void setClubBrotherhood(String clubBrotherhood) {
        this.clubBrotherhood = clubBrotherhood;
    }

    public String getMagikType() {
        return magikType;
    }

    public void setMagikType(String magikType) {
        this.magikType = magikType;
    }

    public String getMagik() {
        return magik;
    }

    public void setMagik(String magik) {
        this.magik = magik;
    }

    public String getSopMagik() {
        return sopMagik;
    }

    public void setSopMagik(String sopMagik) {
        this.sopMagik = sopMagik;
    }

    public Integer getStr() {
        return str;
    }

    public void setStr(Integer str) {
        this.str = str;
    }

    public Integer getAgil() {
        return agil;
    }

    public void setAgil(Integer agil) {
        this.agil = agil;
    }

    public Integer getKonst() {
        return konst;
    }

    public void setKonst(Integer konst) {
        this.konst = konst;
    }

    public Integer getStamin() {
        return stamin;
    }

    public void setStamin(Integer stamin) {
        this.stamin = stamin;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getIntel() {
        return intel;
    }

    public void setIntel(Integer intel) {
        this.intel = intel;
    }

    public Integer getWisp() {
        return wisp;
    }

    public void setWisp(Integer wisp) {
        this.wisp = wisp;
    }

    public Integer getWill() {
        return will;
    }

    public void setWill(Integer will) {
        this.will = will;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getCharm() {
        return charm;
    }

    public void setCharm(Integer charm) {
        this.charm = charm;
    }

    public StatusOfMagicianEnum getStatus() {
        return status;
    }

    public void setStatus(StatusOfMagicianEnum status) {
        this.status = status;
    }

    public Integer getPsy() {
        return psy;
    }

    public void setPsy(Integer psy) {
        this.psy = psy;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Magician that = (Magician) o;

        if (id != that.id) return false;
        if (sex != that.sex) return false;
        if (course != that.course) return false;
        if (room != that.room) return false;
        if (block != that.block) return false;
        if (groupNumber != that.groupNumber) return false;
        if (flow != that.flow) return false;
        if (cod != that.cod) return false;
        if (str != that.str) return false;
        if (agil != that.agil) return false;
        if (konst != that.konst) return false;
        if (stamin != that.stamin) return false;
        if (speed != that.speed) return false;
        if (intel != that.intel) return false;
        if (wisp != that.wisp) return false;
        if (will != that.will) return false;
        if (exp != that.exp) return false;
        if (charm != that.charm) return false;
        if (status != that.status) return false;
        if (psy != that.psy) return false;
        if (money != that.money) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (clubBrotherhood != null ? !clubBrotherhood.equals(that.clubBrotherhood) : that.clubBrotherhood != null)
            return false;
        if (magikType != null ? !magikType.equals(that.magikType) : that.magikType != null) return false;
        if (magik != null ? !magik.equals(that.magik) : that.magik != null) return false;
        if (sopMagik != null ? !sopMagik.equals(that.sopMagik) : that.sopMagik != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.intValue();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + course;
        result = 31 * result + room;
        result = 31 * result + block;
        result = 31 * result + groupNumber;
        result = 31 * result + flow;
        result = 31 * result + cod;
        result = 31 * result + (clubBrotherhood != null ? clubBrotherhood.hashCode() : 0);
        result = 31 * result + (magikType != null ? magikType.hashCode() : 0);
        result = 31 * result + (magik != null ? magik.hashCode() : 0);
        result = 31 * result + (sopMagik != null ? sopMagik.hashCode() : 0);
        result = 31 * result + str;
        result = 31 * result + agil;
        result = 31 * result + konst;
        result = 31 * result + stamin;
        result = 31 * result + speed;
        result = 31 * result + intel;
        result = 31 * result + wisp;
        result = 31 * result + will;
        result = 31 * result + exp;
        result = 31 * result + charm;
        result = 31 * result + psy;
        result = 31 * result + money;
        return result;
    }
}
