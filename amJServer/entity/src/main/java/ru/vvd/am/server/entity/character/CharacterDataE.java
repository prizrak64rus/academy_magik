package ru.vvd.am.server.entity.character;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "character_data", schema = "public", catalog = "magic_academy_01")
public class CharacterDataE {
    private int idCharactrerData;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String alias;
    private boolean sex;
    private Integer age;
    private String placeOfBirth;
    private Integer height;
    private Integer weight;
    private String look;
    private int money;
    private int reputation;
    private int celebrity;
    private int relation;

    @Id
    @Column(name = "id_charactrer_data", nullable = false)
    public int getIdCharactrerData() {
        return idCharactrerData;
    }

    public void setIdCharactrerData(int idCharactrerData) {
        this.idCharactrerData = idCharactrerData;
    }

    @Basic
    @Column(name = "first_name", nullable = true, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "second_name", nullable = true, length = 255)
    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Basic
    @Column(name = "last_name", nullable = true, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "short_name", nullable = true, length = 255)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Basic
    @Column(name = "alias", nullable = true, length = 255)
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Basic
    @Column(name = "sex", nullable = false)
    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "age", nullable = true)
    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Basic
    @Column(name = "place_of_birth", nullable = true, length = 255)
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    @Basic
    @Column(name = "height", nullable = true)
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @Basic
    @Column(name = "weight", nullable = true)
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "look", nullable = true, length = 255)
    public String getLook() {
        return look;
    }

    public void setLook(String look) {
        this.look = look;
    }

    @Basic
    @Column(name = "money", nullable = false)
    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Basic
    @Column(name = "reputation", nullable = false)
    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    @Basic
    @Column(name = "celebrity", nullable = false)
    public int getCelebrity() {
        return celebrity;
    }

    public void setCelebrity(int celebrity) {
        this.celebrity = celebrity;
    }

    @Basic
    @Column(name = "relation", nullable = false)
    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterDataE that = (CharacterDataE) o;

        if (idCharactrerData != that.idCharactrerData) return false;
        if (sex != that.sex) return false;
        if (money != that.money) return false;
        if (reputation != that.reputation) return false;
        if (celebrity != that.celebrity) return false;
        if (relation != that.relation) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (secondName != null ? !secondName.equals(that.secondName) : that.secondName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (shortName != null ? !shortName.equals(that.shortName) : that.shortName != null) return false;
        if (alias != null ? !alias.equals(that.alias) : that.alias != null) return false;
        if (age != null ? !age.equals(that.age) : that.age != null) return false;
        if (placeOfBirth != null ? !placeOfBirth.equals(that.placeOfBirth) : that.placeOfBirth != null) return false;
        if (height != null ? !height.equals(that.height) : that.height != null) return false;
        if (weight != null ? !weight.equals(that.weight) : that.weight != null) return false;
        if (look != null ? !look.equals(that.look) : that.look != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idCharactrerData;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (shortName != null ? shortName.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        result = 31 * result + (sex ? 1 : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (placeOfBirth != null ? placeOfBirth.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (look != null ? look.hashCode() : 0);
        result = 31 * result + money;
        result = 31 * result + reputation;
        result = 31 * result + celebrity;
        result = 31 * result + relation;
        return result;
    }
}
