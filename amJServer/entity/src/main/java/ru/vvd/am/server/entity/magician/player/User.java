package ru.vvd.am.server.entity.magician.player; /**
 * Created by Prizrak on 02.10.2017.
 */

import ru.vvd.am.server.entity.magician.Player;

import javax.persistence.*;
import java.util.List;

@Entity // This tells Hibernate to make a table out of this class
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String name;

    private String password;

    private Integer readGrand;

    private Integer writeGrand;

    @Transient
    private List<String> playersName;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="magician_id", referencedColumnName="id")
    private Player player = new Player();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getReadGrand() {
        return readGrand;
    }

    public void setReadGrand(Integer readGrand) {
        this.readGrand = readGrand;
    }

    public Integer getWriteGrand() {
        return writeGrand;
    }

    public void setWriteGrand(Integer writeGrand) {
        this.writeGrand = writeGrand;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<String> getPlayersName() {
        return playersName;
    }

    public void setPlayersName(List<String> playersName) {
        this.playersName = playersName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User that = (User) o;

        if (readGrand != that.readGrand) return false;
        if (writeGrand != that.writeGrand) return false;
        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + readGrand;
        result = 31 * result + writeGrand;
        result = 31 * result + id.intValue();
        return result;
    }


}