package ru.vvd.am.server.utils;

/**
 * Created by vvashkevich on 09.01.2018.
 */
public interface RestURL {
    public static String USER_C = "/user";
    public static String USER_C_AUTCH = "/autch";

    public static String PLAYER_C = "/player";

    public static String MESSAGES_C = "/messages";
    public static String MESSAGES_SEND_C = "/send";
    public static String MESSAGES_CHECK_C = "/check";

    public static String ART_C = "/art";
    public static String ART_SEND_C = "/send";
    public static String ART_CHECK_C = "/check";
}
