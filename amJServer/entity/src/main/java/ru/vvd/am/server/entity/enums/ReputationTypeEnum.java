package ru.vvd.am.server.entity.enums;

/**
 * Created by Prizrak on 08.01.2018.
 */
public enum  ReputationTypeEnum {
    KURS1,
    KURS2,
    KURS3,
    KURS4,
    KURS5,
    KURS6,
    KURS7,
    KURS0,
    TEACHERS,
    CLUB,
    DIS_COM,
    SOVET
}
