package ru.vvd.am.server.entity.magician;

import ru.vvd.am.server.entity.magician.player.*;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
@Table(name = "personality")
public class Player extends Magician {
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<Skill> skills;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<Spell> spells;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<Inventory> inventories;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<ReputationPlayer> reputationPlayers;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<Notes> notes;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<Messages> messages;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name="magician_id")
    private Set<MoneyOperationHistory> moneyOperationHistories;

    public Set<MoneyOperationHistory> getMoneyOperationHistories() {
        return moneyOperationHistories;
    }

    public void setMoneyOperationHistories(Set<MoneyOperationHistory> moneyOperationHistories) {
        this.moneyOperationHistories = moneyOperationHistories;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Set<Spell> getSpells() {
        return spells;
    }

    public void setSpells(Set<Spell> spells) {
        this.spells = spells;
    }

    public Set<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(Set<Inventory> inventories) {
        this.inventories = inventories;
    }

    public Set<ReputationPlayer> getReputationPlayers() {
        return reputationPlayers;
    }

    public void setReputationPlayers(Set<ReputationPlayer> reputationPlayers) {
        this.reputationPlayers = reputationPlayers;
    }

    public Set<Notes> getNotes() {
        return notes;
    }

    public void setNotes(Set<Notes> notes) {
        this.notes = notes;
    }

    public Set<Messages> getMessages() {
        return messages;
    }

    public void setMessages(Set<Messages> messages) {
        this.messages = messages;
    }
}
