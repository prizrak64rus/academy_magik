package ru.vvd.am.server.utils;

/**
 * Created by Prizrak on 08.01.2018.
 */
public class DateParser {

    public static String integerToDate(Integer date){
        int dey;
        int month;
        int year;
        dey=date%28;
        month=(date/28)%12;
        year=(date/28)/12;
        return dey+"."+month+"."+year;
    }

}
