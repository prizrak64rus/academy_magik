package ru.vvd.am.server.entity.catalog;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "catalog_psychotypes", schema = "public", catalog = "magic_academy_01")
public class CatalogPsychotypesE {
    private int idPsychotype;
    private String name;

    @Id
    @Column(name = "id_psychotype", nullable = false)
    public int getIdPsychotype() {
        return idPsychotype;
    }

    public void setIdPsychotype(int idPsychotype) {
        this.idPsychotype = idPsychotype;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CatalogPsychotypesE that = (CatalogPsychotypesE) o;

        if (idPsychotype != that.idPsychotype) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPsychotype;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
