package ru.vvd.am.server.entity.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "character_status", schema = "public", catalog = "magic_academy_01")
public class CharacterStatusE {
    private int idCharacterStatus;

    @Id
    @Column(name = "id_character_status", nullable = false)
    public int getIdCharacterStatus() {
        return idCharacterStatus;
    }

    public void setIdCharacterStatus(int idCharacterStatus) {
        this.idCharacterStatus = idCharacterStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterStatusE that = (CharacterStatusE) o;

        if (idCharacterStatus != that.idCharacterStatus) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return idCharacterStatus;
    }
}
