package ru.vvd.am.server.entity.magician.player;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;
    private String description;
    private Integer lvl;
   /* @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="magician_id")
    private personality personality;*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLvl() {
        return lvl;
    }

    public void setLvl(Integer lvl) {
        this.lvl = lvl;
    }

   /* public personality getMagician() {
        return personality;
    }

    public void setMagician(personality personality) {
        this.personality = personality;
    }/*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Skill that = (Skill) o;

        if (id != that.id) return false;
        if (lvl != that.lvl) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + lvl;
        return result;
    }
}
