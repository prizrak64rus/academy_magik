package ru.vvd.am.server.entity.magician.player;

import ru.vvd.am.server.entity.enums.ReputationTypeEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
@Entity
public class ReputationPlayer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @Enumerated(EnumType.STRING)
    private ReputationTypeEnum type;
    private Integer maleRep;
    private Integer femaleRep;
    private Integer totalRep;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ReputationTypeEnum getType() {
        return type;
    }

    public void setType(ReputationTypeEnum type) {
        this.type = type;
    }

    public Integer getMaleRep() {
        return maleRep;
    }

    public void setMaleRep(Integer maleRep) {
        this.maleRep = maleRep;
    }

    public Integer getFemaleRep() {
        return femaleRep;
    }

    public void setFemaleRep(Integer femaleRep) {
        this.femaleRep = femaleRep;
    }

    public Integer getTotalRep() {
        return totalRep;
    }

    public void setTotalRep(Integer totalRep) {
        this.totalRep = totalRep;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReputationPlayer that = (ReputationPlayer) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (type != that.type) return false;
        if (maleRep != null ? !maleRep.equals(that.maleRep) : that.maleRep != null) return false;
        if (femaleRep != null ? !femaleRep.equals(that.femaleRep) : that.femaleRep != null) return false;
        return totalRep != null ? totalRep.equals(that.totalRep) : that.totalRep == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (maleRep != null ? maleRep.hashCode() : 0);
        result = 31 * result + (femaleRep != null ? femaleRep.hashCode() : 0);
        result = 31 * result + (totalRep != null ? totalRep.hashCode() : 0);
        return result;
    }
}
