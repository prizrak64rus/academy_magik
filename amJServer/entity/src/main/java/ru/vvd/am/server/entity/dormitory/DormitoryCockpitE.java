package ru.vvd.am.server.entity.dormitory;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "dormitory_cockpit", schema = "public", catalog = "magic_academy_01")
public class DormitoryCockpitE {
    private int idDormitoryCockpit;
    private int cockpit;

    @Id
    @Column(name = "id_dormitory_cockpit", nullable = false)
    public int getIdDormitoryCockpit() {
        return idDormitoryCockpit;
    }

    public void setIdDormitoryCockpit(int idDormitoryCockpit) {
        this.idDormitoryCockpit = idDormitoryCockpit;
    }

    @Basic
    @Column(name = "cockpit", nullable = false)
    public int getCockpit() {
        return cockpit;
    }

    public void setCockpit(int cockpit) {
        this.cockpit = cockpit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DormitoryCockpitE that = (DormitoryCockpitE) o;

        if (idDormitoryCockpit != that.idDormitoryCockpit) return false;
        if (cockpit != that.cockpit) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDormitoryCockpit;
        result = 31 * result + cockpit;
        return result;
    }
}
