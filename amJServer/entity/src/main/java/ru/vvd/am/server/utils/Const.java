package ru.vvd.am.server.utils;

import java.util.ArrayList;

/**
 * Created by Prizrak on 08.01.2018.
 */
public class Const {
    public static ArrayList<String> MONTH = new ArrayList<String>() {{
        add("Январь");
        add("Февраль");
        add("Март");
        add("Апрель");
        add("Май");
        add("Июнь");
        add("Июль");
        add("Август");
        add("Сентябрь");
        add("Октябрь");
        add("Ноябрь");
        add("Декабрь");
    }};
}
