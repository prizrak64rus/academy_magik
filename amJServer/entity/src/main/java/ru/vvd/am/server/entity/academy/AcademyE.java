package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy", schema = "public", catalog = "magic_academy_01")
public class AcademyE {
    private int idAcademy;
    private String name;

    @Id
    @Column(name = "id_academy", nullable = false)
    public int getIdAcademy() {
        return idAcademy;
    }

    public void setIdAcademy(int idAcademy) {
        this.idAcademy = idAcademy;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyE academyE = (AcademyE) o;

        if (idAcademy != academyE.idAcademy) return false;
        if (name != null ? !name.equals(academyE.name) : academyE.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAcademy;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
