package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy_journal", schema = "public", catalog = "magic_academy_01")
public class AcademyJournalE {
    private int idAcademyJournal;
    private int course;
    private int semester;
    private int mark;
    private Integer markDate;

    @Id
    @Column(name = "id_academy_journal", nullable = false)
    public int getIdAcademyJournal() {
        return idAcademyJournal;
    }

    public void setIdAcademyJournal(int idAcademyJournal) {
        this.idAcademyJournal = idAcademyJournal;
    }

    @Basic
    @Column(name = "course", nullable = false)
    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Basic
    @Column(name = "semester", nullable = false)
    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "mark", nullable = false)
    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Basic
    @Column(name = "mark_date", nullable = true)
    public Integer getMarkDate() {
        return markDate;
    }

    public void setMarkDate(Integer markDate) {
        this.markDate = markDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyJournalE that = (AcademyJournalE) o;

        if (idAcademyJournal != that.idAcademyJournal) return false;
        if (course != that.course) return false;
        if (semester != that.semester) return false;
        if (mark != that.mark) return false;
        if (markDate != null ? !markDate.equals(that.markDate) : that.markDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAcademyJournal;
        result = 31 * result + course;
        result = 31 * result + semester;
        result = 31 * result + mark;
        result = 31 * result + (markDate != null ? markDate.hashCode() : 0);
        return result;
    }
}
