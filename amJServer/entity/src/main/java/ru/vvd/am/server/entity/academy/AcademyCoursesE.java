package ru.vvd.am.server.entity.academy;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "academy_courses", schema = "public", catalog = "magic_academy_01")
public class AcademyCoursesE {
    private int idAcademyCourse;
    private Integer cource;
    private Integer semesterAmmount;

    @Id
    @Column(name = "id_academy_course", nullable = false)
    public int getIdAcademyCourse() {
        return idAcademyCourse;
    }

    public void setIdAcademyCourse(int idAcademyCourse) {
        this.idAcademyCourse = idAcademyCourse;
    }

    @Basic
    @Column(name = "cource", nullable = true)
    public Integer getCource() {
        return cource;
    }

    public void setCource(Integer cource) {
        this.cource = cource;
    }

    @Basic
    @Column(name = "semester_ammount", nullable = true)
    public Integer getSemesterAmmount() {
        return semesterAmmount;
    }

    public void setSemesterAmmount(Integer semesterAmmount) {
        this.semesterAmmount = semesterAmmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcademyCoursesE that = (AcademyCoursesE) o;

        if (idAcademyCourse != that.idAcademyCourse) return false;
        if (cource != null ? !cource.equals(that.cource) : that.cource != null) return false;
        if (semesterAmmount != null ? !semesterAmmount.equals(that.semesterAmmount) : that.semesterAmmount != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idAcademyCourse;
        result = 31 * result + (cource != null ? cource.hashCode() : 0);
        result = 31 * result + (semesterAmmount != null ? semesterAmmount.hashCode() : 0);
        return result;
    }
}
