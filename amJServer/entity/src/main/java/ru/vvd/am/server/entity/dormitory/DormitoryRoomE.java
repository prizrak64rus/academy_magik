package ru.vvd.am.server.entity.dormitory;

import javax.persistence.*;

/**
 * Created by Prizrak on 30.04.2018.
 */
@Entity
@Table(name = "dormitory_room", schema = "public", catalog = "magic_academy_01")
public class DormitoryRoomE {
    private int idDormitoryRoom;
    private Integer room;

    @Id
    @Column(name = "id_dormitory_room", nullable = false)
    public int getIdDormitoryRoom() {
        return idDormitoryRoom;
    }

    public void setIdDormitoryRoom(int idDormitoryRoom) {
        this.idDormitoryRoom = idDormitoryRoom;
    }

    @Basic
    @Column(name = "room", nullable = true)
    public Integer getRoom() {
        return room;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DormitoryRoomE that = (DormitoryRoomE) o;

        if (idDormitoryRoom != that.idDormitoryRoom) return false;
        if (room != null ? !room.equals(that.room) : that.room != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDormitoryRoom;
        result = 31 * result + (room != null ? room.hashCode() : 0);
        return result;
    }
}
