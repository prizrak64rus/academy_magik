package ru.vvd.am.server.repository.additional;

import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.HelpDataE;

/**
 * Created by Prizrak on 01.05.2018.
 */
public interface HelpDataRepository  extends CrudRepository<HelpDataE, Integer> {
}
