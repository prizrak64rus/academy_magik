package ru.vvd.am.server.controller;

import org.springframework.web.bind.annotation.*;
import ru.vvd.am.server.entity.magician.player.Messages;
import ru.vvd.am.server.entity.magician.player.User;
import ru.vvd.am.server.utils.ChatData;
import ru.vvd.am.server.utils.RestURL;

import java.util.List;

/**
 * Created by Prizrak on 11.01.2018.
 */
@RestController
@RequestMapping(path= RestURL.MESSAGES_C)
public class MessagesController {

    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces ="application/json", value = RestURL.MESSAGES_SEND_C)
    public @ResponseBody
    List<Messages> send(@RequestBody final Messages messages) {
        ChatData.getChatData().writeMessages(messages);
        return ChatData.getChatData().checkMessages(messages.getFromWhom());
    }

    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces ="application/json", value = RestURL.MESSAGES_CHECK_C)
    public @ResponseBody
    List<Messages> check(@RequestBody final String str) {
        return ChatData.getChatData().checkMessages(str);
    }
}
