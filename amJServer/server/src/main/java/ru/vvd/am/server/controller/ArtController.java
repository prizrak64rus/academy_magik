package ru.vvd.am.server.controller;

import org.springframework.web.bind.annotation.*;
import ru.vvd.am.server.entity.ArtNotification;
import ru.vvd.am.server.entity.magician.player.Messages;
import ru.vvd.am.server.utils.ArtData;
import ru.vvd.am.server.utils.ChatData;
import ru.vvd.am.server.utils.RestURL;

import java.util.List;

/**
 * Created by Prizrak on 03.05.2018.
 */
@RestController
@RequestMapping(path= RestURL.ART_C)
public class ArtController {
    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces ="application/json", value = RestURL.ART_SEND_C)
    public @ResponseBody
    boolean send(@RequestBody final ArtNotification artNotification) {
        ArtData.getArtData().writeArtNotification(artNotification);
        return true;
    }

    @RequestMapping(method = RequestMethod.POST, consumes="application/json", produces ="application/json", value = RestURL.ART_CHECK_C)
    public @ResponseBody
    List<ArtNotification> check() {
        return ArtData.getArtData().checkArtNotification();
    }
}
