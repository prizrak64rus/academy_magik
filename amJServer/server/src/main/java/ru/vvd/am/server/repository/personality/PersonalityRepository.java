package ru.vvd.am.server.repository.personality;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.personality.PersonalityE;

import java.util.List;

/**
 * Created by Prizrak on 02.05.2018.
 */
public interface PersonalityRepository extends CrudRepository<PersonalityE, Integer> {
    public static final String FIND_PERSONALITY_NAME_FROM_ARTS = "SELECT pe.personalityData.firstName FROM PersonalityE pe WHERE pe.active = true and pe.artExist = true";
    @Query(value = FIND_PERSONALITY_NAME_FROM_ARTS, nativeQuery = false)
    public List<String> findPersonalityNameFromArts();
}
