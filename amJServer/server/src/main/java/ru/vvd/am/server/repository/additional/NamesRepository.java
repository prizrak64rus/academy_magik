package ru.vvd.am.server.repository.additional;

import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.additional.Names;
import ru.vvd.am.server.entity.enums.SexEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
public interface NamesRepository extends CrudRepository<Names, Integer> {

}
