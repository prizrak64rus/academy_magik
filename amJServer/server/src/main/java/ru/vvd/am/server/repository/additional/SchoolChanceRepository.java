package ru.vvd.am.server.repository.additional;

import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.additional.SchoolChance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Prizrak on 08.01.2018.
 */
public interface SchoolChanceRepository extends CrudRepository<SchoolChance, Integer> {
}
