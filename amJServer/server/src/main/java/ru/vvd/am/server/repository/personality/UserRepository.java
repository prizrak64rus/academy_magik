package ru.vvd.am.server.repository.personality;

/**
 * Created by Prizrak on 02.10.2017.
 */

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.UserE;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<UserE, Integer> {

   // @EntityGraph(value = "GroupInfo.detail", type = EntityGraph.EntityGraphType.LOAD)
   UserE findOneByNameAndPassword(String name, String password);

   public static final String FIND_ACTIVE_USER = "SELECT us.personality.personalityData.firstName FROM UserE us WHERE us.personality.active=true ";

    @Query(value = FIND_ACTIVE_USER, nativeQuery = false)
    public List<String> findActiveUser();

}