package ru.vvd.am.server.repository.additional;

import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.additional.DataAM;
import ru.vvd.am.server.entity.enums.TypeDataAMEnum;

import javax.persistence.*;

/**
 * Created by Prizrak on 08.01.2018.
 */
public interface DataAMRepository extends CrudRepository<DataAM, Integer> {

}
