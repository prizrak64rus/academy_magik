package ru.vvd.am.server.entity.db;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "diary", schema = "magik_akademy", catalog = "")
public class DiaryEntity {
    private int id;
    private int idStudent;
    private Date data;
    private String text;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "data")
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DiaryEntity that = (DiaryEntity) o;

        if (id != that.id) return false;
        if (idStudent != that.idStudent) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idStudent;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }
}
