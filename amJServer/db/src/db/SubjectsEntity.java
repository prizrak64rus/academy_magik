package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "subjects", schema = "magik_akademy", catalog = "")
public class SubjectsEntity {
    private int id;
    private int course;
    private String subject;
    private String stat;
    private int page;
    private String specification;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "course")
    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Basic
    @Column(name = "subject")
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Basic
    @Column(name = "stat")
    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    @Basic
    @Column(name = "page")
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Basic
    @Column(name = "specification")
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectsEntity that = (SubjectsEntity) o;

        if (id != that.id) return false;
        if (course != that.course) return false;
        if (page != that.page) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        if (stat != null ? !stat.equals(that.stat) : that.stat != null) return false;
        if (specification != null ? !specification.equals(that.specification) : that.specification != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + course;
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (stat != null ? stat.hashCode() : 0);
        result = 31 * result + page;
        result = 31 * result + (specification != null ? specification.hashCode() : 0);
        return result;
    }
}
