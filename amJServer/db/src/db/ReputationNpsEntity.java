package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "reputation_nps", schema = "magik_akademy", catalog = "")
public class ReputationNpsEntity {
    private int id;
    private int idStudentTo;
    private int attitudes;
    private String status;
    private int idStudentFrom;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student_to")
    public int getIdStudentTo() {
        return idStudentTo;
    }

    public void setIdStudentTo(int idStudentTo) {
        this.idStudentTo = idStudentTo;
    }

    @Basic
    @Column(name = "attitudes")
    public int getAttitudes() {
        return attitudes;
    }

    public void setAttitudes(int attitudes) {
        this.attitudes = attitudes;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "id_student_from")
    public int getIdStudentFrom() {
        return idStudentFrom;
    }

    public void setIdStudentFrom(int idStudentFrom) {
        this.idStudentFrom = idStudentFrom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReputationNpsEntity that = (ReputationNpsEntity) o;

        if (id != that.id) return false;
        if (idStudentTo != that.idStudentTo) return false;
        if (attitudes != that.attitudes) return false;
        if (idStudentFrom != that.idStudentFrom) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idStudentTo;
        result = 31 * result + attitudes;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + idStudentFrom;
        return result;
    }
}
