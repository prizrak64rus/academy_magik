package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "appearance_gamer", schema = "magik_akademy", catalog = "")
public class AppearanceGamerEntity {
    private int idStudent;
    private String description;
    private int growth;
    private double weight;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "growth")
    public int getGrowth() {
        return growth;
    }

    public void setGrowth(int growth) {
        this.growth = growth;
    }

    @Basic
    @Column(name = "weight")
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppearanceGamerEntity that = (AppearanceGamerEntity) o;

        if (idStudent != that.idStudent) return false;
        if (growth != that.growth) return false;
        if (Double.compare(that.weight, weight) != 0) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = idStudent;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + growth;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
