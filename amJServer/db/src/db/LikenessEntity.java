package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "likeness", schema = "magik_akademy", catalog = "")
public class LikenessEntity {
    private int id;
    private String likeness;

    @Id
    @Column(name = "id_")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "likeness")
    public String getLikeness() {
        return likeness;
    }

    public void setLikeness(String likeness) {
        this.likeness = likeness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikenessEntity that = (LikenessEntity) o;

        if (id != that.id) return false;
        if (likeness != null ? !likeness.equals(that.likeness) : that.likeness != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (likeness != null ? likeness.hashCode() : 0);
        return result;
    }
}
