package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "attitudes_character_student", schema = "magik_akademy", catalog = "")
@IdClass(AttitudesCharacterStudentEntityPK.class)
public class AttitudesCharacterStudentEntity {
    private int idStudent;
    private int idCharacter;
    private int attitudes;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Id
    @Column(name = "id_character")
    public int getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(int idCharacter) {
        this.idCharacter = idCharacter;
    }

    @Basic
    @Column(name = "attitudes")
    public int getAttitudes() {
        return attitudes;
    }

    public void setAttitudes(int attitudes) {
        this.attitudes = attitudes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttitudesCharacterStudentEntity that = (AttitudesCharacterStudentEntity) o;

        if (idStudent != that.idStudent) return false;
        if (idCharacter != that.idCharacter) return false;
        if (attitudes != that.attitudes) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + idCharacter;
        result = 31 * result + attitudes;
        return result;
    }
}
