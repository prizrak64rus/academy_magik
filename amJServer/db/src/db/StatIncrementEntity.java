package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "stat_increment", schema = "magik_akademy", catalog = "")
public class StatIncrementEntity {
    private int idStudent;
    private String rootStat;
    private int incrementRoot;
    private int incrementLite;
    private int incrementMin;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "root_stat")
    public String getRootStat() {
        return rootStat;
    }

    public void setRootStat(String rootStat) {
        this.rootStat = rootStat;
    }

    @Basic
    @Column(name = "increment_root")
    public int getIncrementRoot() {
        return incrementRoot;
    }

    public void setIncrementRoot(int incrementRoot) {
        this.incrementRoot = incrementRoot;
    }

    @Basic
    @Column(name = "increment_lite")
    public int getIncrementLite() {
        return incrementLite;
    }

    public void setIncrementLite(int incrementLite) {
        this.incrementLite = incrementLite;
    }

    @Basic
    @Column(name = "increment_min")
    public int getIncrementMin() {
        return incrementMin;
    }

    public void setIncrementMin(int incrementMin) {
        this.incrementMin = incrementMin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatIncrementEntity that = (StatIncrementEntity) o;

        if (idStudent != that.idStudent) return false;
        if (incrementRoot != that.incrementRoot) return false;
        if (incrementLite != that.incrementLite) return false;
        if (incrementMin != that.incrementMin) return false;
        if (rootStat != null ? !rootStat.equals(that.rootStat) : that.rootStat != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + (rootStat != null ? rootStat.hashCode() : 0);
        result = 31 * result + incrementRoot;
        result = 31 * result + incrementLite;
        result = 31 * result + incrementMin;
        return result;
    }
}
