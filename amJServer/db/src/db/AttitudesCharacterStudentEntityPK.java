package ru.vvd.am.server.entity.db;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Prizrak on 03.10.2017.
 */
public class AttitudesCharacterStudentEntityPK implements Serializable {
    private int idStudent;
    private int idCharacter;

    @Column(name = "id_student")
    @Id
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Column(name = "id_character")
    @Id
    public int getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(int idCharacter) {
        this.idCharacter = idCharacter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttitudesCharacterStudentEntityPK that = (AttitudesCharacterStudentEntityPK) o;

        if (idStudent != that.idStudent) return false;
        if (idCharacter != that.idCharacter) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + idCharacter;
        return result;
    }
}
