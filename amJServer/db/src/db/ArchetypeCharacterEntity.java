package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "archetype_character", schema = "magik_akademy", catalog = "")
public class ArchetypeCharacterEntity {
    private int id;
    private String archetype;
    private String type;
    private String description;
    private int bravery;
    private int aggression;
    private int lust;
    private int joy;
    private int determination;
    private int prudence;
    private int openness;
    private int rationality;
    private int integrity;
    private int groupNumber;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "archetype")
    public String getArchetype() {
        return archetype;
    }

    public void setArchetype(String archetype) {
        this.archetype = archetype;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "bravery")
    public int getBravery() {
        return bravery;
    }

    public void setBravery(int bravery) {
        this.bravery = bravery;
    }

    @Basic
    @Column(name = "aggression")
    public int getAggression() {
        return aggression;
    }

    public void setAggression(int aggression) {
        this.aggression = aggression;
    }

    @Basic
    @Column(name = "lust")
    public int getLust() {
        return lust;
    }

    public void setLust(int lust) {
        this.lust = lust;
    }

    @Basic
    @Column(name = "joy")
    public int getJoy() {
        return joy;
    }

    public void setJoy(int joy) {
        this.joy = joy;
    }

    @Basic
    @Column(name = "determination")
    public int getDetermination() {
        return determination;
    }

    public void setDetermination(int determination) {
        this.determination = determination;
    }

    @Basic
    @Column(name = "prudence")
    public int getPrudence() {
        return prudence;
    }

    public void setPrudence(int prudence) {
        this.prudence = prudence;
    }

    @Basic
    @Column(name = "openness")
    public int getOpenness() {
        return openness;
    }

    public void setOpenness(int openness) {
        this.openness = openness;
    }

    @Basic
    @Column(name = "rationality")
    public int getRationality() {
        return rationality;
    }

    public void setRationality(int rationality) {
        this.rationality = rationality;
    }

    @Basic
    @Column(name = "integrity")
    public int getIntegrity() {
        return integrity;
    }

    public void setIntegrity(int integrity) {
        this.integrity = integrity;
    }

    @Basic
    @Column(name = "group_number")
    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArchetypeCharacterEntity that = (ArchetypeCharacterEntity) o;

        if (id != that.id) return false;
        if (bravery != that.bravery) return false;
        if (aggression != that.aggression) return false;
        if (lust != that.lust) return false;
        if (joy != that.joy) return false;
        if (determination != that.determination) return false;
        if (prudence != that.prudence) return false;
        if (openness != that.openness) return false;
        if (rationality != that.rationality) return false;
        if (integrity != that.integrity) return false;
        if (groupNumber != that.groupNumber) return false;
        if (archetype != null ? !archetype.equals(that.archetype) : that.archetype != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (archetype != null ? archetype.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + bravery;
        result = 31 * result + aggression;
        result = 31 * result + lust;
        result = 31 * result + joy;
        result = 31 * result + determination;
        result = 31 * result + prudence;
        result = 31 * result + openness;
        result = 31 * result + rationality;
        result = 31 * result + integrity;
        result = 31 * result + groupNumber;
        return result;
    }
}
