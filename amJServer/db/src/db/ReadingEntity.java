package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "reading", schema = "magik_akademy", catalog = "")
public class ReadingEntity {
    private int idEvaluation;
    private int pageLearning;

    @Id
    @Column(name = "id_evaluation")
    public int getIdEvaluation() {
        return idEvaluation;
    }

    public void setIdEvaluation(int idEvaluation) {
        this.idEvaluation = idEvaluation;
    }

    @Basic
    @Column(name = "page_learning")
    public int getPageLearning() {
        return pageLearning;
    }

    public void setPageLearning(int pageLearning) {
        this.pageLearning = pageLearning;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReadingEntity that = (ReadingEntity) o;

        if (idEvaluation != that.idEvaluation) return false;
        if (pageLearning != that.pageLearning) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idEvaluation;
        result = 31 * result + pageLearning;
        return result;
    }
}
