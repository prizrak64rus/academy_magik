package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "reputation", schema = "magik_akademy", catalog = "")
public class ReputationEntity {
    private int id;
    private int idStudent;
    private int type;
    private int male;
    private int female;
    private int all;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "male")
    public int getMale() {
        return male;
    }

    public void setMale(int male) {
        this.male = male;
    }

    @Basic
    @Column(name = "female")
    public int getFemale() {
        return female;
    }

    public void setFemale(int female) {
        this.female = female;
    }

    @Basic
    @Column(name = "all")
    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReputationEntity that = (ReputationEntity) o;

        if (id != that.id) return false;
        if (idStudent != that.idStudent) return false;
        if (type != that.type) return false;
        if (male != that.male) return false;
        if (female != that.female) return false;
        if (all != that.all) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idStudent;
        result = 31 * result + type;
        result = 31 * result + male;
        result = 31 * result + female;
        result = 31 * result + all;
        return result;
    }
}
