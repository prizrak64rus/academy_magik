package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "appearance_student", schema = "magik_akademy", catalog = "")
public class AppearanceStudentEntity {
    private int idStudent;
    private int eyeShape;
    private int eyeColor;
    private int hairLength;
    private int hairColor;
    private int bodyType;
    private int weight;
    private int boobs;
    private int hairstyle;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "eye_shape")
    public int getEyeShape() {
        return eyeShape;
    }

    public void setEyeShape(int eyeShape) {
        this.eyeShape = eyeShape;
    }

    @Basic
    @Column(name = "\neye_color")
    public int getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(int eyeColor) {
        this.eyeColor = eyeColor;
    }

    @Basic
    @Column(name = "hair_length")
    public int getHairLength() {
        return hairLength;
    }

    public void setHairLength(int hairLength) {
        this.hairLength = hairLength;
    }

    @Basic
    @Column(name = "\nhair_color")
    public int getHairColor() {
        return hairColor;
    }

    public void setHairColor(int hairColor) {
        this.hairColor = hairColor;
    }

    @Basic
    @Column(name = "\nbody_type")
    public int getBodyType() {
        return bodyType;
    }

    public void setBodyType(int bodyType) {
        this.bodyType = bodyType;
    }

    @Basic
    @Column(name = "weight")
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "boobs")
    public int getBoobs() {
        return boobs;
    }

    public void setBoobs(int boobs) {
        this.boobs = boobs;
    }

    @Basic
    @Column(name = "\nhairstyle")
    public int getHairstyle() {
        return hairstyle;
    }

    public void setHairstyle(int hairstyle) {
        this.hairstyle = hairstyle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppearanceStudentEntity that = (AppearanceStudentEntity) o;

        if (idStudent != that.idStudent) return false;
        if (eyeShape != that.eyeShape) return false;
        if (eyeColor != that.eyeColor) return false;
        if (hairLength != that.hairLength) return false;
        if (hairColor != that.hairColor) return false;
        if (bodyType != that.bodyType) return false;
        if (weight != that.weight) return false;
        if (boobs != that.boobs) return false;
        if (hairstyle != that.hairstyle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + eyeShape;
        result = 31 * result + eyeColor;
        result = 31 * result + hairLength;
        result = 31 * result + hairColor;
        result = 31 * result + bodyType;
        result = 31 * result + weight;
        result = 31 * result + boobs;
        result = 31 * result + hairstyle;
        return result;
    }
}
