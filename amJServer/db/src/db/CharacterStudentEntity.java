package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "character_student", schema = "magik_akademy", catalog = "")
public class CharacterStudentEntity {
    private int idStudent;
    private int character1;
    private int character2;
    private int character3;
    private int character4;
    private int character5;
    private int character6;
    private int character7;
    private int character8;
    private int likeness;
    private int mask;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "character1")
    public int getCharacter1() {
        return character1;
    }

    public void setCharacter1(int character1) {
        this.character1 = character1;
    }

    @Basic
    @Column(name = "character2")
    public int getCharacter2() {
        return character2;
    }

    public void setCharacter2(int character2) {
        this.character2 = character2;
    }

    @Basic
    @Column(name = "character3")
    public int getCharacter3() {
        return character3;
    }

    public void setCharacter3(int character3) {
        this.character3 = character3;
    }

    @Basic
    @Column(name = "character4")
    public int getCharacter4() {
        return character4;
    }

    public void setCharacter4(int character4) {
        this.character4 = character4;
    }

    @Basic
    @Column(name = "character5")
    public int getCharacter5() {
        return character5;
    }

    public void setCharacter5(int character5) {
        this.character5 = character5;
    }

    @Basic
    @Column(name = "character6")
    public int getCharacter6() {
        return character6;
    }

    public void setCharacter6(int character6) {
        this.character6 = character6;
    }

    @Basic
    @Column(name = "character7")
    public int getCharacter7() {
        return character7;
    }

    public void setCharacter7(int character7) {
        this.character7 = character7;
    }

    @Basic
    @Column(name = "character8")
    public int getCharacter8() {
        return character8;
    }

    public void setCharacter8(int character8) {
        this.character8 = character8;
    }

    @Basic
    @Column(name = "likeness")
    public int getLikeness() {
        return likeness;
    }

    public void setLikeness(int likeness) {
        this.likeness = likeness;
    }

    @Basic
    @Column(name = "mask")
    public int getMask() {
        return mask;
    }

    public void setMask(int mask) {
        this.mask = mask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterStudentEntity that = (CharacterStudentEntity) o;

        if (idStudent != that.idStudent) return false;
        if (character1 != that.character1) return false;
        if (character2 != that.character2) return false;
        if (character3 != that.character3) return false;
        if (character4 != that.character4) return false;
        if (character5 != that.character5) return false;
        if (character6 != that.character6) return false;
        if (character7 != that.character7) return false;
        if (character8 != that.character8) return false;
        if (likeness != that.likeness) return false;
        if (mask != that.mask) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + character1;
        result = 31 * result + character2;
        result = 31 * result + character3;
        result = 31 * result + character4;
        result = 31 * result + character5;
        result = 31 * result + character6;
        result = 31 * result + character7;
        result = 31 * result + character8;
        result = 31 * result + likeness;
        result = 31 * result + mask;
        return result;
    }
}
