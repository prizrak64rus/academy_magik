package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "valuation", schema = "magik_akademy", catalog = "")
public class ValuationEntity {
    private int idEvaluation;
    private int trimester;
    private int valuation;
    private int id;

    @Basic
    @Column(name = "id_evaluation")
    public int getIdEvaluation() {
        return idEvaluation;
    }

    public void setIdEvaluation(int idEvaluation) {
        this.idEvaluation = idEvaluation;
    }

    @Basic
    @Column(name = "trimester")
    public int getTrimester() {
        return trimester;
    }

    public void setTrimester(int trimester) {
        this.trimester = trimester;
    }

    @Basic
    @Column(name = "valuation")
    public int getValuation() {
        return valuation;
    }

    public void setValuation(int valuation) {
        this.valuation = valuation;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValuationEntity that = (ValuationEntity) o;

        if (idEvaluation != that.idEvaluation) return false;
        if (trimester != that.trimester) return false;
        if (valuation != that.valuation) return false;
        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idEvaluation;
        result = 31 * result + trimester;
        result = 31 * result + valuation;
        result = 31 * result + id;
        return result;
    }
}
