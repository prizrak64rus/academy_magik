package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "character_ban_student", schema = "magik_akademy", catalog = "")
public class CharacterBanStudentEntity {
    private int id;
    private int idCharacter;
    private int idBanCharacter;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_character")
    public int getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(int idCharacter) {
        this.idCharacter = idCharacter;
    }

    @Basic
    @Column(name = "id_ban_character")
    public int getIdBanCharacter() {
        return idBanCharacter;
    }

    public void setIdBanCharacter(int idBanCharacter) {
        this.idBanCharacter = idBanCharacter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterBanStudentEntity that = (CharacterBanStudentEntity) o;

        if (id != that.id) return false;
        if (idCharacter != that.idCharacter) return false;
        if (idBanCharacter != that.idBanCharacter) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idCharacter;
        result = 31 * result + idBanCharacter;
        return result;
    }
}
