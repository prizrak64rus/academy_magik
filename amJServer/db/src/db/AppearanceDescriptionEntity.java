package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "appearance_description", schema = "magik_akademy", catalog = "")
public class AppearanceDescriptionEntity {
    private int id;
    private int type;
    private String description;
    private int sex;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "sex")
    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppearanceDescriptionEntity that = (AppearanceDescriptionEntity) o;

        if (id != that.id) return false;
        if (type != that.type) return false;
        if (sex != that.sex) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + type;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + sex;
        return result;
    }
}
