package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "school_relation", schema = "magik_akademy", catalog = "")
public class SchoolRelationEntity {
    private String school;
    private String ban;
    private String percent5;
    private String percent15;
    private String percent30;
    private String percent50;
    private int id;

    @Basic
    @Column(name = "school")
    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    @Basic
    @Column(name = "ban")
    public String getBan() {
        return ban;
    }

    public void setBan(String ban) {
        this.ban = ban;
    }

    @Basic
    @Column(name = "percent5")
    public String getPercent5() {
        return percent5;
    }

    public void setPercent5(String percent5) {
        this.percent5 = percent5;
    }

    @Basic
    @Column(name = "percent15")
    public String getPercent15() {
        return percent15;
    }

    public void setPercent15(String percent15) {
        this.percent15 = percent15;
    }

    @Basic
    @Column(name = "percent30")
    public String getPercent30() {
        return percent30;
    }

    public void setPercent30(String percent30) {
        this.percent30 = percent30;
    }

    @Basic
    @Column(name = "percent50")
    public String getPercent50() {
        return percent50;
    }

    public void setPercent50(String percent50) {
        this.percent50 = percent50;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SchoolRelationEntity that = (SchoolRelationEntity) o;

        if (id != that.id) return false;
        if (school != null ? !school.equals(that.school) : that.school != null) return false;
        if (ban != null ? !ban.equals(that.ban) : that.ban != null) return false;
        if (percent5 != null ? !percent5.equals(that.percent5) : that.percent5 != null) return false;
        if (percent15 != null ? !percent15.equals(that.percent15) : that.percent15 != null) return false;
        if (percent30 != null ? !percent30.equals(that.percent30) : that.percent30 != null) return false;
        if (percent50 != null ? !percent50.equals(that.percent50) : that.percent50 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = school != null ? school.hashCode() : 0;
        result = 31 * result + (ban != null ? ban.hashCode() : 0);
        result = 31 * result + (percent5 != null ? percent5.hashCode() : 0);
        result = 31 * result + (percent15 != null ? percent15.hashCode() : 0);
        result = 31 * result + (percent30 != null ? percent30.hashCode() : 0);
        result = 31 * result + (percent50 != null ? percent50.hashCode() : 0);
        result = 31 * result + id;
        return result;
    }
}
