package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "likeness_preference", schema = "magik_akademy", catalog = "")
public class LikenessPreferenceEntity {
    private int id;
    private int idLikeness;
    private String preference;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_likeness")
    public int getIdLikeness() {
        return idLikeness;
    }

    public void setIdLikeness(int idLikeness) {
        this.idLikeness = idLikeness;
    }

    @Basic
    @Column(name = "preference")
    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikenessPreferenceEntity that = (LikenessPreferenceEntity) o;

        if (id != that.id) return false;
        if (idLikeness != that.idLikeness) return false;
        if (preference != null ? !preference.equals(that.preference) : that.preference != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idLikeness;
        result = 31 * result + (preference != null ? preference.hashCode() : 0);
        return result;
    }
}
