package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "evaluation", schema = "magik_akademy", catalog = "")
public class EvaluationEntity {
    private int id;
    private int idStudent;
    private double mod;
    private int subjectMod;
    private String lesson;
    private double essay;
    private int exchangeRate;
    private int exam;
    private double itog;
    private int trimesterEvaluation1;
    private int trimesterEvaluation2;
    private int trimesterEvaluation3;
    private int course;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "mod")
    public double getMod() {
        return mod;
    }

    public void setMod(double mod) {
        this.mod = mod;
    }

    @Basic
    @Column(name = "subject_mod")
    public int getSubjectMod() {
        return subjectMod;
    }

    public void setSubjectMod(int subjectMod) {
        this.subjectMod = subjectMod;
    }

    @Basic
    @Column(name = "lesson")
    public String getLesson() {
        return lesson;
    }

    public void setLesson(String lesson) {
        this.lesson = lesson;
    }

    @Basic
    @Column(name = "essay")
    public double getEssay() {
        return essay;
    }

    public void setEssay(double essay) {
        this.essay = essay;
    }

    @Basic
    @Column(name = "exchange_rate")
    public int getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(int exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    @Basic
    @Column(name = "exam")
    public int getExam() {
        return exam;
    }

    public void setExam(int exam) {
        this.exam = exam;
    }

    @Basic
    @Column(name = "itog")
    public double getItog() {
        return itog;
    }

    public void setItog(double itog) {
        this.itog = itog;
    }

    @Basic
    @Column(name = "trimester_evaluation_1")
    public int getTrimesterEvaluation1() {
        return trimesterEvaluation1;
    }

    public void setTrimesterEvaluation1(int trimesterEvaluation1) {
        this.trimesterEvaluation1 = trimesterEvaluation1;
    }

    @Basic
    @Column(name = "trimester_evaluation_2")
    public int getTrimesterEvaluation2() {
        return trimesterEvaluation2;
    }

    public void setTrimesterEvaluation2(int trimesterEvaluation2) {
        this.trimesterEvaluation2 = trimesterEvaluation2;
    }

    @Basic
    @Column(name = "trimester_evaluation_3")
    public int getTrimesterEvaluation3() {
        return trimesterEvaluation3;
    }

    public void setTrimesterEvaluation3(int trimesterEvaluation3) {
        this.trimesterEvaluation3 = trimesterEvaluation3;
    }

    @Basic
    @Column(name = "course")
    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EvaluationEntity that = (EvaluationEntity) o;

        if (id != that.id) return false;
        if (idStudent != that.idStudent) return false;
        if (Double.compare(that.mod, mod) != 0) return false;
        if (subjectMod != that.subjectMod) return false;
        if (Double.compare(that.essay, essay) != 0) return false;
        if (exchangeRate != that.exchangeRate) return false;
        if (exam != that.exam) return false;
        if (Double.compare(that.itog, itog) != 0) return false;
        if (trimesterEvaluation1 != that.trimesterEvaluation1) return false;
        if (trimesterEvaluation2 != that.trimesterEvaluation2) return false;
        if (trimesterEvaluation3 != that.trimesterEvaluation3) return false;
        if (course != that.course) return false;
        if (lesson != null ? !lesson.equals(that.lesson) : that.lesson != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + idStudent;
        temp = Double.doubleToLongBits(mod);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + subjectMod;
        result = 31 * result + (lesson != null ? lesson.hashCode() : 0);
        temp = Double.doubleToLongBits(essay);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + exchangeRate;
        result = 31 * result + exam;
        temp = Double.doubleToLongBits(itog);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + trimesterEvaluation1;
        result = 31 * result + trimesterEvaluation2;
        result = 31 * result + trimesterEvaluation3;
        result = 31 * result + course;
        return result;
    }
}
