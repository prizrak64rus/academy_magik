package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "lern_mod_gamer", schema = "magik_akademy", catalog = "")
public class LernModGamerEntity {
    private int idStudent;
    private int lermMod;

    @Id
    @Column(name = "id_student")
    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    @Basic
    @Column(name = "lerm_mod")
    public int getLermMod() {
        return lermMod;
    }

    public void setLermMod(int lermMod) {
        this.lermMod = lermMod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LernModGamerEntity that = (LernModGamerEntity) o;

        if (idStudent != that.idStudent) return false;
        if (lermMod != that.lermMod) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idStudent;
        result = 31 * result + lermMod;
        return result;
    }
}
