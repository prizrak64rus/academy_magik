package ru.vvd.am.server.entity.db;

import javax.persistence.*;

/**
 * Created by Prizrak on 03.10.2017.
 */
@Entity
@Table(name = "valuation_nps", schema = "magik_akademy", catalog = "")
public class ValuationNpsEntity {
    private int id;
    private int idEvaluation;
    private int valuationSum;
    private int valuationCount;
    private int trimester;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_evaluation")
    public int getIdEvaluation() {
        return idEvaluation;
    }

    public void setIdEvaluation(int idEvaluation) {
        this.idEvaluation = idEvaluation;
    }

    @Basic
    @Column(name = "valuation_sum")
    public int getValuationSum() {
        return valuationSum;
    }

    public void setValuationSum(int valuationSum) {
        this.valuationSum = valuationSum;
    }

    @Basic
    @Column(name = "valuation_count")
    public int getValuationCount() {
        return valuationCount;
    }

    public void setValuationCount(int valuationCount) {
        this.valuationCount = valuationCount;
    }

    @Basic
    @Column(name = "trimester")
    public int getTrimester() {
        return trimester;
    }

    public void setTrimester(int trimester) {
        this.trimester = trimester;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValuationNpsEntity that = (ValuationNpsEntity) o;

        if (id != that.id) return false;
        if (idEvaluation != that.idEvaluation) return false;
        if (valuationSum != that.valuationSum) return false;
        if (valuationCount != that.valuationCount) return false;
        if (trimester != that.trimester) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idEvaluation;
        result = 31 * result + valuationSum;
        result = 31 * result + valuationCount;
        result = 31 * result + trimester;
        return result;
    }
}
