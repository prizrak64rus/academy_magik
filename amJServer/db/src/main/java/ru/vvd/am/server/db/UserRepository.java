package ru.vvd.am.server.db;

/**
 * Created by Prizrak on 02.10.2017.
 */

import org.springframework.data.repository.CrudRepository;
import ru.vvd.am.server.entity.User;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Long> {

}