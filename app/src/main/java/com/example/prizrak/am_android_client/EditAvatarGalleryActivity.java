package com.example.prizrak.am_android_client;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.prizrak.am_android_client.adapter.BaseAvatarAdapter;
import com.example.prizrak.am_android_client.adapter.BaseFolderAdapter;
import com.example.prizrak.am_android_client.adapter.EditDeleteAvatarAdapter;
import com.example.prizrak.am_android_client.adapter.EditDragAvatarAdapter;
import com.example.prizrak.am_android_client.adapter.EditFolderAdapter;
import com.example.prizrak.am_android_client.subview.PreGenActivity;
import com.example.prizrak.am_android_client.utils.AvatarItem;
import com.example.prizrak.am_android_client.utils.DBHelper;
import com.example.prizrak.am_android_client.utils.DataAPP;
import com.example.prizrak.am_android_client.utils.FolderItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.vvd.am.server.utils.FileUtils;

/**
 * Created by Prizrak on 04.05.2018.
 */

public class EditAvatarGalleryActivity extends PreGenActivity implements IEditAvaterGallery {

    ArrayList<FolderItem> folderItems = new ArrayList<>();
    FolderItem lastFolderItem;
    FolderItem tmpFolderItem;
    BaseFolderAdapter folderAdapter;
    RecyclerView folderRecyclerView;
    HashMap<FolderItem, List<AvatarItem>> itemListHashMap = new HashMap<>();

    RecyclerView avatarRecyclerView;
    BaseAvatarAdapter avatarAdapterBase;
    BaseAvatarAdapter tmpAdapterBase;

    Button addFolderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_avatar_gallery);

        addFolderButton = (Button) findViewById(R.id.addFolder);

        initFolder();
        initImageGalery();
    }

    private void initImageGalery() {
        avatarRecyclerView = (RecyclerView) findViewById(R.id.imagegallery);
        avatarRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);
        avatarRecyclerView.setLayoutManager(layoutManager);
        ArrayList<AvatarItem> avatarItems = prepareDataImageGalery();
        avatarAdapterBase = new EditDragAvatarAdapter(getApplicationContext(), avatarItems, this, this);
        avatarRecyclerView.setAdapter(avatarAdapterBase);
    }

    private void initFolder() {
        folderRecyclerView = (RecyclerView) findViewById(R.id.folder);
        folderRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        folderRecyclerView.setLayoutManager(layoutManager);
        ArrayList<FolderItem> items = prepareDataFolder();
        folderAdapter = new EditFolderAdapter(getApplicationContext(), items, this, this);
        folderRecyclerView.setAdapter(folderAdapter);
    }

    private ArrayList<FolderItem> prepareDataFolder() {
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DBHelper.TABLE_GALLERY_FOLDER, null, null, null, null, null, "id");
        if (c.getCount() != 0 && c.moveToFirst())
            do {
                FolderItem item = new FolderItem();
                item.setFolder_name(c.getString(c.getColumnIndex(DBHelper.COLUMN_GALLERY_FOLDER_NAME)));
                item.setId(c.getInt(c.getColumnIndex(DBHelper.COLUMN_GALLERY_FOLDER_PRIORITY)));
                lastFolderItem = item;
                folderItems.add(item);
            } while (c.moveToNext());
        if (lastFolderItem == null) {
            lastFolderItem = new FolderItem();
            lastFolderItem.setId(0);
        }
        dbHelper.close();
        return folderItems;
    }

    private ArrayList<AvatarItem> prepareDataImageGalery() {

        ArrayList<AvatarItem> theimage = new ArrayList<>();
        AvatarItem avatarItem = new AvatarItem();
        avatarItem.setImage_title(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName());
        avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName()));
        theimage.add(avatarItem);

        avatarItem = new AvatarItem();
        avatarItem.setImage_title(FileUtils.TIME_OUT);
        avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(FileUtils.TIME_OUT));
        theimage.add(avatarItem);
        if (DataAPP.user.getName().equals("fux")) {
            avatarItem = new AvatarItem();
            avatarItem.setImage_title(DataAPP.FON_S);
            avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(DataAPP.FON_S));
            theimage.add(avatarItem);
        }
        for (String key : RootActivity.bitmapMap.keySet()) {
            if (DataAPP.user.getUserList().contains(key) || key.equals(FileUtils.TIME_OUT) || key.equals(DataAPP.FON_S))
                continue;
            avatarItem = new AvatarItem();
            avatarItem.setImage_title(key);
            avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(key));
            theimage.add(avatarItem);
        }

        {
            dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = db.query(DBHelper.TABLE_ITEM_GALLERY_FOLDER, null, null, null, null, null, DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER);
            int keyFolder = -1;
            List<AvatarItem> avatarItems = new ArrayList<>();
            if (c.getCount() != 0 && c.moveToFirst()) {
                if (keyFolder == -1) {
                    keyFolder = c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER));
                }
                do {
                    if (keyFolder != c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER))) {
                        for (FolderItem folderItem : folderItems) {
                            if (folderItem.getId() == keyFolder) {
                                itemListHashMap.put(folderItem, avatarItems);
                                break;
                            }
                        }
                        keyFolder = c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER));
                        avatarItems = new ArrayList<>();
                    }
                    AvatarItem item = new AvatarItem();
                    item.setImage_title(c.getString(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ART_NAME)));
                    item.setImage_Bitmap(RootActivity.bitmapMap.get(item.getImage_title()));
                    avatarItems.add(item);
                } while (c.moveToNext());
            }
            for (FolderItem folderItem : folderItems) {
                if (folderItem.getId() == keyFolder) {
                    itemListHashMap.put(folderItem, avatarItems);
                    break;
                }
            }
            dbHelper.close();
        }
        return theimage;
    }

    private void saveDataFolder() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE_GALLERY_FOLDER, null, null);
        for (FolderItem item : folderItems) {
            ContentValues cv = new ContentValues();
            cv.put(DBHelper.COLUMN_GALLERY_FOLDER_NAME, item.getFolder_name());
            cv.put(DBHelper.COLUMN_GALLERY_FOLDER_PRIORITY, item.getId());
            long rowID = db.insert(DBHelper.TABLE_GALLERY_FOLDER, null, cv);
        }
        dbHelper.close();
    }

    private void saveDataAvatar() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE_ITEM_GALLERY_FOLDER, null, null);
        for (FolderItem item : folderItems) {
            List<AvatarItem> avatarItems = itemListHashMap.get(item);
            if (avatarItems == null) continue;
            for (AvatarItem avatarItem : avatarItems) {
                ContentValues cv = new ContentValues();
                cv.put(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ART_NAME, avatarItem.getImage_title());
                cv.put(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER, item.getId());
                long rowID = db.insert(DBHelper.TABLE_ITEM_GALLERY_FOLDER, null, cv);
            }
        }
        dbHelper.close();
    }

    public void onClickAddFolder(View v) {
        if ("break".equals(v.getTag())) {
            folderAction(false, null);
        } else {
            addFolder();
        }
    }

    private void addFolder() {
        final AlertDialog.Builder ratingdialog = new AlertDialog.Builder(this);
        View linearlayout = getLayoutInflater().inflate(R.layout.dialog_enter_name, null);
        ratingdialog.setView(linearlayout);
        ratingdialog.setCancelable(false);
        final EditText editText = (EditText) linearlayout.findViewById(R.id.enter_name);

        ratingdialog.setNeutralButton("Готово",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //txtView.setText(String.valueOf(rating.getRating()));
                        if (editText.getText().toString().trim().isEmpty()) {
                            ratingdialog.setTitle("Нормальное название, а не гавно!");
                        } else {
                            FolderItem item = new FolderItem();
                            item.setFolder_name(editText.getText().toString().trim());
                            item.setId(lastFolderItem.getId() + 1);
                            lastFolderItem = item;
                            folderItems.add(item);
                            folderAdapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    }
                })

                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        ratingdialog.create();
        ratingdialog.show();
    }

    @Override
    public void onBackPressed() {
        saveDataFolder();
        saveDataAvatar();
        finish();

    }

    @Override
    public void onSelectFolder(final FolderItem folderItem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                folderAction(true, folderItem);
            }
        });
    }

    @Override
    public void onAddAvatarFromFolder(FolderItem folderItem, String avatarName) {
        List<AvatarItem> avatarItems = itemListHashMap.get(folderItem);
        if (avatarItems == null) {
            avatarItems = new ArrayList<>();
            itemListHashMap.put(folderItem, avatarItems);
        }
        AvatarItem avatarItem = new AvatarItem();
        avatarItem.setImage_title(avatarName);
        avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(avatarName));
        if (!avatarItems.contains(avatarItem)) {
            avatarItems.add(avatarItem);
        }
    }

    @Override
    public void onDeleteFolder(FolderItem folderItem) {
        itemListHashMap.remove(folderItem);
        folderItems.remove(folderItem);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE_GALLERY_FOLDER, DBHelper.COLUMN_GALLERY_FOLDER_PRIORITY + "=" + folderItem.getId(), null);
        db.delete(DBHelper.TABLE_ITEM_GALLERY_FOLDER, DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER + "=" + folderItem.getId(), null);
        dbHelper.close();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                folderAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onDeleteAvatar(AvatarItem avatarItem) {
        itemListHashMap.get(tmpFolderItem).remove(avatarItem);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DBHelper.TABLE_ITEM_GALLERY_FOLDER, DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER + "=" + tmpFolderItem.getId()
                + " AND " + DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ART_NAME + "='" + avatarItem.getImage_title() + "'", null);
        dbHelper.close();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tmpAdapterBase.notifyDataSetChanged();
            }
        });
    }

    protected void folderAction(boolean isSelect, FolderItem folderItem) {
        if (isSelect) {
            List<AvatarItem> avatarItems = itemListHashMap.get(folderItem);
            if (avatarItems == null) {
                avatarItems = new ArrayList<>();
                itemListHashMap.put(folderItem, avatarItems);
            }
            tmpAdapterBase = new EditDeleteAvatarAdapter(getApplicationContext(), avatarItems, this, this, folderItem.getId());
            tmpFolderItem=folderItem;
            folderRecyclerView.setVisibility(View.INVISIBLE);
            //folderRecyclerView.setVisibility(View.INVISIBLE);
            setTitleActionBar(folderItem.getFolder_name());
            addFolderButton.setText("Назад");
            addFolderButton.setTag("break");
            avatarRecyclerView.setAdapter(tmpAdapterBase);
        } else {
            tmpAdapterBase = null;
            tmpFolderItem=null;
            folderRecyclerView.setVisibility(View.VISIBLE);
            setTitleActionBar("Академия магии...");
            addFolderButton.setText("Добавить");
            addFolderButton.setTag("add");
            avatarRecyclerView.setAdapter(avatarAdapterBase);
        }
    }
}
