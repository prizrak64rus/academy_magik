package com.example.prizrak.am_android_client.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.example.prizrak.am_android_client.RootActivity;
import com.example.prizrak.am_android_client.utils.AvatarItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;

import java.util.List;

public class EditDeleteAvatarAdapter extends BaseAvatarAdapter {

    protected int idFolder;

    public EditDeleteAvatarAdapter(Context context, List<AvatarItem> galleryList, Activity activity, IEditAvaterGallery editAvaterGallery, int idFolder) {
        super(context, galleryList, activity, editAvaterGallery);
        this.idFolder = idFolder;
    }

    protected void initClickAction(final ViewHolder viewHolder,final int i) {
        viewHolder.img.setOnLongClickListener(new View.OnLongClickListener() {

            // Defines the one method for the interface, which is called when the View is long-clicked
            public boolean onLongClick(View v) {

                final AlertDialog.Builder ratingdialog = new AlertDialog.Builder(activity);
                ratingdialog.setTitle("Удалить: "+galleryList.get(i).getImage_title()+"?");
                ratingdialog.setPositiveButton("Удалить",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editAvaterGallery.onDeleteAvatar(galleryList.get(i));
                            }
                        })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                ratingdialog.create();
                ratingdialog.show();
                return true;
            }
        });
    }
}
