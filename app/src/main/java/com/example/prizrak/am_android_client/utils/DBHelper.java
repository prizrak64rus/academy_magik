package com.example.prizrak.am_android_client.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public static final String COLUMN_ID = "id";

    public static final String COLUMN_AUTCH_LOGIN = "login";
    public static final String COLUMN_AUTCH_PASSWORD = "password";
    public static final String COLUMN_AUTCH_IP = "ip";
    public static final String TABLE_AUTCH = "t_autch";

    public static final String COLUMN_GALLERY_FOLDER_PRIORITY = "priority";
    public static final String COLUMN_GALLERY_FOLDER_NAME = "name";
    public static final String TABLE_GALLERY_FOLDER = "gallery_folder";

    public static final String COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER = "id_folder";
    public static final String COLUMN_ITEM_GALLERY_FOLDER_ART_NAME = "name";
    public static final String TABLE_ITEM_GALLERY_FOLDER = "item_gallery_folder";



    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "academy_magik", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // создаем таблицу с полями
        db.execSQL(QUERY_CREATE_TABLE_AUTCH);
        db.execSQL(QUERY_CREATE_TABLE_GALLERY_FOLDER);
        db.execSQL(QUERY_CREATE_TABLE_ITEM_GALLERY_FOLDER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion == 1){
            db.execSQL(QUERY_CREATE_TABLE_GALLERY_FOLDER);
            db.execSQL(QUERY_CREATE_TABLE_ITEM_GALLERY_FOLDER);
        }

    }

    private String QUERY_CREATE_TABLE_AUTCH = "create table t_autch ("
            + "id integer primary key autoincrement,"
            + "login text,"
            + "ip text,"
            + "password text"
            + ");";

    private String QUERY_CREATE_TABLE_GALLERY_FOLDER ="create table gallery_folder ("
            + "id integer primary key autoincrement,"
            + "name text,"
            + "priority integer"
            + ");";

    private String QUERY_CREATE_TABLE_ITEM_GALLERY_FOLDER ="create table item_gallery_folder ("
            + "id integer primary key autoincrement,"
            + "name text,"
            + "id_folder integer"
            + ");";
}
