package com.example.prizrak.am_android_client.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.prizrak.am_android_client.R;
import com.example.prizrak.am_android_client.utils.FolderItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;

import java.util.ArrayList;

/**
 * Created by Prizrak on 04.05.2018.
 */

public class BaseFolderAdapter extends RecyclerView.Adapter<BaseFolderAdapter.ViewHolder> {
    protected ArrayList<FolderItem> itemList;
    protected Context context;
    protected Activity activity;
    protected IEditAvaterGallery editAvaterGallery;

    public BaseFolderAdapter(Context context, ArrayList<FolderItem> itemList, Activity activity, IEditAvaterGallery editAvaterGallery) {
        this.itemList = itemList;
        this.context = context;
        this.activity = activity;
        this.editAvaterGallery = editAvaterGallery;
    }

    @Override
    public BaseFolderAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_folder, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BaseFolderAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.title.setText(itemList.get(i).getFolder_name());
        viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        viewHolder.img.setTag(itemList.get(i).getFolder_name());
        initClickAction(viewHolder.folder_view, i);
    }

    protected void initClickAction(final View folder_view, final int i) {
        folder_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAvaterGallery.onSelectFolder(itemList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView img;
        private View folder_view;

        public ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            img = (ImageView) view.findViewById(R.id.img);
            folder_view = view.findViewById(R.id.folder_view);
        }
    }
}