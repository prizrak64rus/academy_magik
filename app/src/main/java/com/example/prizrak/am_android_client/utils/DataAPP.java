package com.example.prizrak.am_android_client.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import ru.vvd.am.server.entity.ArtNotification;
import ru.vvd.am.server.entity.UserE;
import ru.vvd.am.server.entity.magician.player.Messages;

/**
 * Created by Prizrak on 10.01.2018.
 */

public class DataAPP {
    public static UserE user;
    public static String url;
    public static String toWhom;
    public static ThreadMessages threadMessages;
    public static String FON_S="Cтолб";

    private static DataAPP dataAPP;
    private static Map<String,CopyOnWriteArrayList<Messages>> messagesMap = new HashMap<>();
    private static List<ArtNotification> artNotifications = new ArrayList<ArtNotification>();
    private final Object lockMes = new Object();

    private DataAPP() {
    }
    public static DataAPP getDataAPP(){
        if(dataAPP==null){
            dataAPP=new DataAPP();
        }
        return dataAPP;
    }

    public void writeArtNotification(List<ArtNotification> ars){
        artNotifications=ars;
    }

    public List<ArtNotification> checkArtNotification(){
        return  artNotifications;
    }

    public void writeMessages(Messages message){
        synchronized (lockMes)        {
            CopyOnWriteArrayList<Messages> messages = messagesMap.get(message.getFromWhom());
            if(messages==null){
                messages = new CopyOnWriteArrayList<Messages>();
                messagesMap.put(message.getToWhom(),messages);
            }
            messages.add(message);
        }
    }

    public CopyOnWriteArrayList<Messages> checkMessages(String toWhom){
        synchronized (lockMes)        {
            CopyOnWriteArrayList<Messages> messages = messagesMap.get(toWhom);
            if(messages==null){
                messages = new CopyOnWriteArrayList<>();
                messagesMap.put(toWhom,messages);
                return messages;
            }
            return messages;
        }
    }
}
