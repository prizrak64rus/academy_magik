package com.example.prizrak.am_android_client.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.view.View;

import com.example.prizrak.am_android_client.RootActivity;
import com.example.prizrak.am_android_client.utils.AvatarItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;
import com.example.prizrak.am_android_client.utils.MyDragShadowBuilder;

import java.util.List;

public class EditDragAvatarAdapter extends BaseAvatarAdapter {

    public EditDragAvatarAdapter(Context context, List<AvatarItem> galleryList, Activity activity, IEditAvaterGallery editAvaterGallery) {
        super(context, galleryList, activity, editAvaterGallery);
    }

    protected void initClickAction(final ViewHolder viewHolder, int i) {
        viewHolder.img.setOnLongClickListener(new View.OnLongClickListener() {

            // Defines the one method for the interface, which is called when the View is long-clicked
            public boolean onLongClick(View v) {

                // Create a new ClipData.
                // This is done in two steps to provide clarity. The convenience method
                // ClipData.newPlainText() can create a plain text ClipData in one step.

                // Create a new ClipData.Item from the ImageView object's tag
                ClipData.Item item = new ClipData.Item(v.getTag().toString());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This will create a new ClipDescription object within the
                // ClipData, and set its MIME type entry to "text/plain"
                ClipData dragData = new ClipData(v.getTag().toString(), new String[1], item);

                // Instantiates the drag shadow builder.
                View.DragShadowBuilder myShadow = new MyDragShadowBuilder(viewHolder.img);

                // Starts the drag

                v.startDrag(dragData,  // the data to be dragged
                        myShadow,  // the drag shadow builder
                        null,      // no need to use local data
                        0          // flags (not currently used, set to 0)
                );
                return true;
            }
        });
    }
}
