package com.example.prizrak.am_android_client;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.prizrak.am_android_client.subview.CastomVerticalTextView;
import com.example.prizrak.am_android_client.utils.DataAPP;
import com.example.prizrak.am_android_client.utils.IMessage;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.vvd.am.server.entity.ArtNotification;
import ru.vvd.am.server.entity.UserE;
import ru.vvd.am.server.utils.RestURL;

/**
 * Created by Prizrak on 03.05.2018.
 */

public class RootActivity extends Activity implements RestURL, IMessage {

    public static Map<String, Bitmap> bitmapMap = new HashMap<>();
    private Map<String, String> imageSootvetstvyMap = new HashMap<>();
    private Map<String, GroupFromUser> groupMap = new HashMap<>();
    private ThreadAvatars thread;
    public static String CHANGE;
    private Object lock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_root);

        UserE user = DataAPP.getDataAPP().user;

        initBitMap();
        initGroupMap();
        Bitmap myBitmap = bitmapMap.get(user.getPersonality().getPersonalityData().getFirstName());
        GroupFromUser group = groupMap.get(user.getPersonality().getPersonalityData().getFirstName());

        group.who.setTextCastom(user.getPersonality().getPersonalityData().getFirstName());
        group.charaster.setTextCastom(user.getPersonality().getPersonalityData().getFirstName());
        group.imageChar.setImageBitmap(myBitmap);
        group.imageChar.setBackgroundColor(Color.BLACK);
        group.view.setVisibility(View.VISIBLE);
        imageSootvetstvyMap.put(user.getPersonality().getPersonalityData().getFirstName()
                , user.getPersonality().getPersonalityData().getFirstName());
        /*group.imageChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SecAct = new Intent(getApplicationContext(), AvatarGalleryActivity.class);
                startActivity(SecAct);
            }
        });*/
        initBMB(group);

        initThread();

        ArtNotification artNotification = new ArtNotification();
        artNotification.setCharaster(user.getPersonality().getPersonalityData().getFirstName());
        artNotification.setWho(user.getPersonality().getPersonalityData().getFirstName());
        HttpRequestTask task = new HttpRequestTask();
        task.setValue(artNotification);
        task.execute();

        DataAPP.getDataAPP().threadMessages.setMessageClass(this);

        startActivity(new Intent(getApplicationContext(), EditAvatarGalleryActivity.class));
    }

    protected void initBMB(GroupFromUser groupFromUser) {
        final BoomMenuButton bmb = (BoomMenuButton) findViewById(R.id.bmb);
        bmb.setButtonEnum(ButtonEnum.SimpleCircle);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_2_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_2_1);
        for (int i = 0; i < bmb.getButtonPlaceEnum().buttonNumber(); i++) {
            SimpleCircleButton.Builder but = new SimpleCircleButton.Builder();
            but.normalColor(Color.BLACK);
            but.pieceColor(Color.BLUE);
            switch (i) {
                case 0:
                    bmb.addBuilder(but
                            .normalImageRes(R.mipmap.ico_edit_gallery));
                    break;
                case 1:
                    bmb.addBuilder(but
                            .normalImageRes(R.mipmap.ico_change_avatar));
                    break;
                default:
                    bmb.addBuilder(but
                            .normalImageRes(R.mipmap.ic_launcher));
                    break;
            }

            but.listener(new OnBMClickListener() {
                @Override
                public void onBoomButtonClick(int index) {
                    switch (index) {
                        case 0:
                            startActivity(new Intent(getApplicationContext(), EditAvatarGalleryActivity.class));
                            break;
                        case 1:
                            startActivity(new Intent(getApplicationContext(), AvatarGalleryActivity.class));
                            break;
                        default:
                            break;
                    }
                    // When the boom-button corresponding this builder is clicked.
                    Toast.makeText(RootActivity.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                }
            });

        }
        //bmb.setVisibility(View.GONE);
        groupFromUser.imageChar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bmb.boom();
            }
        });
    }

    @Override
    public void onMessage() {

    }

    protected void initBitMap() {
        Map<String, byte[]> avatarMap = DataAPP.user.getAvatarsMap();
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.fon_s);
        bitmapMap.put(DataAPP.FON_S, largeIcon);
        for (String key : avatarMap.keySet()) {
            byte[] avatarByteArr = avatarMap.get(key);
            Bitmap bitmap = BitmapFactory.decodeByteArray(avatarByteArr, 0, avatarByteArr.length);
            bitmapMap.put(key, bitmap);
        }
    }

    protected void initGroupMap() {
        GroupFromUser groupFromUser = getGroupFromUser(groupMap.size());
        groupMap.put(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName(), groupFromUser);
        for (String userName : DataAPP.getDataAPP().user.getUserList()) {
            if (DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName().equals(userName)) {
                continue;
            }
            groupFromUser = getGroupFromUser(groupMap.size());
            groupFromUser.who.setTextCastom(userName);
            groupFromUser.charaster.setTextCastom(userName);
            groupFromUser.imageChar.setTag(userName);
            groupMap.put(userName, groupFromUser);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DataAPP.getDataAPP().threadMessages.setMessageClass(this);
        if (CHANGE != null) {
            ArtNotification artNotification = new ArtNotification();
            artNotification.setCharaster(CHANGE);
            artNotification.setWho(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName());
            HttpRequestTask task = new HttpRequestTask();
            task.setValue(artNotification);
            task.execute();
            CHANGE = null;
        }
        //thread.notify();

    }

    @Override
    protected void onPause() {
        DataAPP.getDataAPP().threadMessages.setMessageClass(null);
        super.onPause();
    }

    private ThreadAvatars initThread() {
        if (thread != null) {
            thread.stop();
        }
        thread = new ThreadAvatars("thsTH");

        //thread.setListener(this);
        thread.start();
        return thread;
    }

    protected void changeAvatars(List<ArtNotification> artNotifications) {
        synchronized (lock) {
            for (ArtNotification artNotification : artNotifications) {
                addArt(artNotification);
            }
        }
    }

    protected void addArt(ArtNotification artNotification) {
        if (artNotification.getCharaster()
                .equals(imageSootvetstvyMap.get(artNotification.getWho()))) {
            return;
        } else {
            imageSootvetstvyMap.put(artNotification.getWho(), artNotification.getCharaster());
        }
        final GroupFromUser groupFromUser = groupMap.get(artNotification.getWho());
        final String charaster = artNotification.getCharaster();
        final ImageView imageViewFinal = groupFromUser.imageChar;
        final Bitmap bitmapFinal = bitmapMap.get(artNotification.getCharaster());
        final String who = artNotification.getWho();
        imageViewFinal.post(new Runnable() {
            public void run() {
                imageViewFinal.setImageBitmap(bitmapFinal);
                groupFromUser.charaster.setTextCastom(charaster);
                if (groupFromUser.isFirstSelect) {
                    if (!DataAPP.user.getPersonality().getPersonalityData().getFirstName().equals(who)) {
                        groupFromUser.imageChar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DataAPP.toWhom = v.getTag().toString();
                                Intent SecAct = new Intent(getApplicationContext(), ChatRomActivity.class);
                                startActivity(SecAct);
                            }
                        });
                    }
                    groupFromUser.isFirstSelect = false;
                    groupFromUser.view.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    protected GroupFromUser getGroupFromUser(int size) {
        switch (size) {
            case 0: {
                return new GroupFromUser((View) findViewById(R.id.group0));
            }
            case 1: {
                return new GroupFromUser((View) findViewById(R.id.group1));
            }
            case 2: {
                return new GroupFromUser((View) findViewById(R.id.group2));
            }
            case 3: {
                return new GroupFromUser((View) findViewById(R.id.group3));
            }
            default: {
                return null;
            }
        }
    }

    public class GroupFromUser {
        public CastomVerticalTextView who;
        public CastomVerticalTextView charaster;
        public ImageView imageChar;
        public View view;
        public boolean isFirstSelect = true;

        public GroupFromUser(View view) {
            this.view = view;
            view.setVisibility(View.INVISIBLE);
            charaster = (CastomVerticalTextView) view.findViewById(R.id.charaster);
            who = (CastomVerticalTextView) view.findViewById(R.id.who);
            imageChar = (ImageView) view.findViewById(R.id.imageChar);
            imageChar.setBackgroundColor(Color.BLACK);
        }
    }


    class ThreadAvatars extends HandlerThread implements RestURL {
        public ThreadAvatars(String name) {
            super(name);
        }

        public void run() {
            while (true) {
                try {
                    sleep(1000);
                    final String url = DataAPP.url + ART_C + ART_CHECK_C;
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    restTemplate.getMessageConverters()
                            .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);
                    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                    HttpEntity<String> entity = new HttpEntity<>(headers);

                    List<ArtNotification> messagesList = restTemplate.postForObject(url, entity, List.class);
                    ObjectMapper mapper = new ObjectMapper();
                    if (messagesList != null && messagesList.size() > 0) {
                        messagesList = mapper.convertValue(messagesList, new TypeReference<List<ArtNotification>>() {
                        });
                    }
                    if (messagesList != null) {
                        DataAPP.getDataAPP().writeArtNotification(messagesList);
                        changeAvatars(messagesList);
                    }
                } catch (Exception e) {
                    yield();
                }
            }
        }
    }


    private class HttpRequestTask extends AsyncTask<Void, Void, Void> {
        private ArtNotification artNotification;

        public void setValue(ArtNotification artNotification) {
            this.artNotification = artNotification;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                final String url = DataAPP.url + ART_C + ART_SEND_C;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                //  restTemplate.getForObject(url, User.class);
                Boolean result = restTemplate.postForObject(url, artNotification, Boolean.class);
            } catch (Exception e) {
                Log.e("CharacteristicsActivity", e.getMessage(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void voidV) {
            System.out.println("tesy");
            // TextView greetingIdText = (TextView) findViewById(R.id.idTest);
            // greetingIdText.setText(user.getName());
            /*if(user==null){
                errLogin();
            } else {
                DataAPP.user=user;
                okLogin();
            }*/
            //rh(user.getPlayer());

        }

    }
}
