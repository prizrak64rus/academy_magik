package com.example.prizrak.am_android_client.utils;

import android.graphics.Bitmap;

import java.util.Objects;

/**
 * Created by Prizrak on 04.05.2018.
 */

public class AvatarItem {

    private String image_title;
    private Bitmap image_bitmap;

    public String getImage_title() {
        return image_title;
    }

    public void setImage_title(String android_version_name) {
        this.image_title = android_version_name;
    }

    public Bitmap getImage_Bitmap() {
        return image_bitmap;
    }

    public void setImage_Bitmap(Bitmap image_bitmap) {
        this.image_bitmap = image_bitmap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AvatarItem that = (AvatarItem) o;
        return Objects.equals(image_title, that.image_title);
    }

    @Override
    public int hashCode() {

        return Objects.hash(image_title);
    }
}
