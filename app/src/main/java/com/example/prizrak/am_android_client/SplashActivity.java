package com.example.prizrak.am_android_client;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Prizrak on 10.01.2018.
 */

public class SplashActivity extends Activity {

    private final int MY_PERMISSIONS_REQUEST_RECORD_AUDIO =1;
    private final int MY_PERMISSIONS_REQUEST_MODIFY_AUDIO_SETTINGS=2;

    private boolean RECORD_AUDIO = false;
    private boolean MODIFY_AUDIO_SETTINGS = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //VKSdk.initialize(getApplicationContext());
        getPermission();
        toNext();
    }

    private void toNext(){
        if(RECORD_AUDIO&&MODIFY_AUDIO_SETTINGS) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void getPermission(){
        RECORD_AUDIO = checkPrem(MY_PERMISSIONS_REQUEST_RECORD_AUDIO, Manifest.permission.RECORD_AUDIO);
        MODIFY_AUDIO_SETTINGS = checkPrem(MY_PERMISSIONS_REQUEST_MODIFY_AUDIO_SETTINGS, Manifest.permission.MODIFY_AUDIO_SETTINGS);
    }

    private boolean checkPrem(int request, String permission){
        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        request);
                return  false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        try {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_RECORD_AUDIO: {
                    // If request is cancelled, the result arrays are empty.
                    RECORD_AUDIO = checkRequestPermissionsResult(grantResults);
                    return;
                }
                case MY_PERMISSIONS_REQUEST_MODIFY_AUDIO_SETTINGS: {
                    // If request is cancelled, the result arrays are empty.
                    MODIFY_AUDIO_SETTINGS = checkRequestPermissionsResult(grantResults);
                    return;
                }
                // other 'case' lines to check for other
                // permissions this app might request.
            }
            toNext();
        } catch (Exception e){

        }
    }

    private boolean checkRequestPermissionsResult(int[] grantResults){
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            return  true;
        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            return false;
        }
    }
}
