package com.example.prizrak.am_android_client.subview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;

/**
 * Created by Prizrak on 16.05.2018.
 */

public class CastomVerticalTextView extends android.support.v7.widget.AppCompatTextView {
    final boolean topDown;
    TextPaint textPaint = getPaint();

    public CastomVerticalTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        final int gravity = getGravity();
        if (Gravity.isVertical(gravity)
                && (gravity & Gravity.VERTICAL_GRAVITY_MASK) == Gravity.BOTTOM) {
            setGravity((gravity & Gravity.HORIZONTAL_GRAVITY_MASK)
                    | Gravity.TOP);
            topDown = false;
        } else {
            topDown = true;
        }
    }

    public void setTextCastom(CharSequence text){
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<text.length();i++){
            sb.append(text.charAt(i)).append("\n");
        }
        super.setText(sb.toString());
    }




}
