package com.example.prizrak.am_android_client.subview;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.prizrak.am_android_client.R;
import com.example.prizrak.am_android_client.utils.DBHelper;

public class PreGenActivity extends AppCompatActivity {

    protected DBHelper dbHelper;
    TextView titleActionBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createCustomActionBar();
    }

    private void createCustomActionBar(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
        View view =getSupportActionBar().getCustomView();

        ImageButton imageButton= (ImageButton)view.findViewById(R.id.action_bar_back);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        titleActionBarView = (TextView) view.findViewById(R.id.action_bar_title);
        //ImageButton imageButton2= (ImageButton)view.findViewById(R.id.action_bar_forward);

      /*  imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Forward Button is clicked",Toast.LENGTH_LONG).show();
            }
        });*/
    }

    protected void setTitleActionBar(String value){
        titleActionBarView.setText(value);
    }
}
