package com.example.prizrak.am_android_client.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.DragEvent;
import android.view.View;

import com.example.prizrak.am_android_client.utils.FolderItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;

import java.util.ArrayList;

public class EditFolderAdapter extends BaseFolderAdapter {

    public EditFolderAdapter(Context context, ArrayList<FolderItem> itemList, Activity activity, IEditAvaterGallery editAvaterGallery) {
        super(context, itemList, activity, editAvaterGallery);
    }

    @Override
    protected void initClickAction(final View folder_view, final int i) {
        folder_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAvaterGallery.onSelectFolder(itemList.get(i));
            }
        });
        folder_view.setOnLongClickListener(new View.OnLongClickListener() {

            // Defines the one method for the interface, which is called when the View is long-clicked
            public boolean onLongClick(View v) {

                final AlertDialog.Builder ratingdialog = new AlertDialog.Builder(activity);
                ratingdialog.setTitle("Удалить папку: "+itemList.get(i).getFolder_name()+"?");
                ratingdialog.setPositiveButton("Удалить",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                editAvaterGallery.onDeleteFolder(itemList.get(i));
                            }
                        })
                        .setNegativeButton("Отмена",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                ratingdialog.create();
                ratingdialog.show();
                return true;
            }
        });
        folder_view.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // v.getBackground().setColorFilter(new LightingColorFilter(Color.BLUE,0));
                        break;
                    case DragEvent.ACTION_DROP:
                        editAvaterGallery.onAddAvatarFromFolder(itemList.get(i),
                                event.getClipData().getItemAt(0).getText().toString());
                        break;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        folder_view.setBackgroundColor(Color.parseColor("#6567F5"));//#FFFFFF
                        break;

                    case DragEvent.ACTION_DRAG_EXITED:
                        folder_view.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        break;

                    case DragEvent.ACTION_DRAG_LOCATION:
                        break;

                    case DragEvent.ACTION_DRAG_ENDED:
                        folder_view.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        break;

                    default:
                        break;
                }
                return true;
            }
        });
    }
}
