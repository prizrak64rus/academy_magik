package com.example.prizrak.am_android_client;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ru.vvd.am.server.entity.magician.Magician;
import ru.vvd.am.server.utils.RestURL;

/**
 * Created by Prizrak on 10.01.2018.
 */

public class StatActivity extends Activity implements RestURL{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_stat);
       /* android.support.design.widget.NavigationView
        android.support.design.widget.NavigationView.*/
       // fillData(DataAPP.user.getPlayer());
    }

    public void fillData(Magician magician){
        ((TextView) findViewById(R.id.Pl_Name)).setText(magician.getName());
        fillStat(magician);
    }

    public void fillStat(Magician magician){
        ((TextView) findViewById(R.id.StatStr)).setText(magician.getStr().toString());
        ((TextView) findViewById(R.id.StatAgil)).setText(magician.getAgil().toString());
        ((TextView) findViewById(R.id.StatStamin)).setText(magician.getStamin().toString());
        ((TextView) findViewById(R.id.StatSpeed)).setText(magician.getSpeed().toString());
        ((TextView) findViewById(R.id.StatKonst)).setText(magician.getKonst().toString());
        ((TextView) findViewById(R.id.StatIntel)).setText(magician.getIntel().toString());
        ((TextView) findViewById(R.id.StatWisp)).setText(magician.getWisp().toString());
        ((TextView) findViewById(R.id.StatWill)).setText(magician.getWill().toString());
        ((TextView) findViewById(R.id.StatExp)).setText(magician.getExp().toString());
        ((TextView) findViewById(R.id.StatCharm)).setText(magician.getCharm().toString());

    }

    public void actionMastM(View v){
       // Intent SecAct = new Intent(getApplicationContext(), ChatGlobalActivity.class);
       // startActivity(SecAct);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
