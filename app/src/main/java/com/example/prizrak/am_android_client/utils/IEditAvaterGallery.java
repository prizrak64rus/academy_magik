package com.example.prizrak.am_android_client.utils;

public interface IEditAvaterGallery {

    void onSelectFolder(FolderItem folderItem);

    void onAddAvatarFromFolder(FolderItem folderItem, String avatarName);

    void onDeleteFolder(FolderItem folderItem);

    void onDeleteAvatar(AvatarItem avatarItem);
}
