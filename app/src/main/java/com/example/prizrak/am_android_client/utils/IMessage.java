package com.example.prizrak.am_android_client.utils;

/**
 * Created by Prizrak on 04.05.2018.
 */

public interface IMessage {
    void onMessage();
}
