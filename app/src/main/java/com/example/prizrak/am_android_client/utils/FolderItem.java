package com.example.prizrak.am_android_client.utils;

import java.util.Objects;

public class FolderItem {

    private String folder_name;
    private int id;

    public FolderItem(String folder_name, int id) {
        this.folder_name = folder_name;
        this.id = id;
    }

    public FolderItem() {
    }

    public String getFolder_name() {
        return folder_name;
    }

    public void setFolder_name(String folder_name) {
        this.folder_name = folder_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderItem item = (FolderItem) o;
        return id == item.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
