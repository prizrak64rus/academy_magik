package com.example.prizrak.am_android_client;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.prizrak.am_android_client.adapter.MessageListAdapter;
import com.example.prizrak.am_android_client.utils.DataAPP;
import com.example.prizrak.am_android_client.utils.IMessage;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import ru.vvd.am.server.entity.magician.player.Messages;
import ru.vvd.am.server.utils.RestURL;

/**
 * Created by Prizrak on 11.01.2018.
 */

public class ChatRomActivity extends Activity implements RestURL, IMessage {

    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_chat_rum);
        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        mMessageAdapter = new MessageListAdapter(this, DataAPP.getDataAPP().checkMessages(DataAPP.toWhom));
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageRecycler.setAdapter(mMessageAdapter);
        DataAPP.getDataAPP().threadMessages.setMessageClass(this);
    }

    @Override
    protected void onStop(){
        DataAPP.getDataAPP().threadMessages.setMessageClass(null);
        super.onStop();
    }

    public void sendMessage(View v){
        if(((TextView) findViewById(R.id.edittext_chatbox)).getText().toString().isEmpty())
            return;
        Messages m = new Messages(DataAPP.user.getPersonality().getPersonalityData().getFirstName(),0
                ,((TextView) findViewById(R.id.edittext_chatbox)).getText().toString(),DataAPP.toWhom);
        DataAPP.getDataAPP().checkMessages(DataAPP.toWhom).add(m);
        HttpRequestTaskSM taskSM = new HttpRequestTaskSM();
        taskSM.setValue(m);
        taskSM.execute();
        ((TextView) findViewById(R.id.edittext_chatbox)).setText("");
        onMessage();
    }

    @Override
    public void onMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessageAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("000000000000000blaaaaaaaa");
        //thread.notify();

    }

    public void fillData(){
        String plName = DataAPP.user.getPersonality().getPersonalityData().getFirstName();
        ((TextView) findViewById(R.id.Pl_Name)).setText(plName);
    }


    private class HttpRequestTaskSM extends AsyncTask<Void, Void, List<Messages>> {
        private Messages messages;
        public void setValue(Messages messages){
            this.messages = messages;
        }
        @Override
        protected List doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                //  restTemplate.getForObject(url, User.class);
                ObjectMapper mapper = new ObjectMapper();
                List messagesList=null;
                if(messages!=null) {
                    final String url =  DataAPP.url + MESSAGES_C + MESSAGES_SEND_C;
                    messagesList = restTemplate.postForObject(url, messages, List.class);
                } else {
                    final String url =  DataAPP.url + MESSAGES_C + MESSAGES_CHECK_C;
                    messagesList = restTemplate.postForObject(url, DataAPP.user.getPersonality().getPersonalityData().getFirstName(), List.class);
                }
                if(messagesList!=null&&messagesList.size()>0) {
                    List<Messages> res = mapper.convertValue(messagesList, new TypeReference<List<Messages>>() {
                    });
                    return res;
                }
                return null;

            } catch (Exception e) {
                Log.e("CharacteristicsActivity", e.getMessage(), e);
            }

            return null;
        }//http://10.0.2.2:8080/user/autch

        @Override
        protected void onPostExecute(List<Messages> messagesList) {
            System.out.println("tesy");
            // TextView greetingIdText = (TextView) findViewById(R.id.idTest);
            // greetingIdText.setText(user.getName());
            if(messagesList==null){
               // errLogin();
            } else {
                for(Messages m : messagesList){
                    DataAPP.getDataAPP().checkMessages(m.getFromWhom()).add(m);
                }
            }
            //rh(user.getPlayer());

        }

    }
}
