package com.example.prizrak.am_android_client.utils;

import com.example.prizrak.am_android_client.ChatRomActivity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

import ru.vvd.am.server.entity.magician.player.Messages;
import ru.vvd.am.server.utils.RestURL;

/**
 * Created by Prizrak on 12.01.2018.
 */

public class ThreadMessages extends Thread implements RestURL, IMessage {
    Object lock = new Object();
    private IMessage messageClass;

    public void run() {
        while (true) {
            try {
                sleep(2000);
                final String url = DataAPP.url + MESSAGES_C + MESSAGES_CHECK_C;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters()
                        .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                HttpEntity<String> entity = new HttpEntity<>(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName(), headers);

                List<Messages> messagesList = restTemplate.postForObject(url, entity, List.class);
                ObjectMapper mapper = new ObjectMapper();
                if(messagesList!=null&&messagesList.size()>0) {
                    messagesList = mapper.convertValue(messagesList, new TypeReference<List<Messages>>() {
                    });
                }
                if (messagesList != null) {
                    for(Messages m : messagesList){
                        DataAPP.getDataAPP().checkMessages(m.getFromWhom()).add(m);
                    }
                    onMessage();
                }
                /*ThreadMessages.HttpRequestTaskSM taskSM = new ThreadMessages.HttpRequestTaskSM();
                taskSM.execute();*/
            } catch (Exception e) {
                yield();
            }
        }
    }

    public void onMessage() {
        synchronized (lock) {
            if (messageClass != null) {
                messageClass.onMessage();
            }
        }
    }

    public void setMessageClass(IMessage messageClass) {
        synchronized (lock) {
            this.messageClass = messageClass;
        }
    }

    /* private class HttpRequestTaskSM extends AsyncTask<Void, Void, List<Messages>> {
        private Messages messages;
        public void setValue(Messages messages){
            this.messages = messages;
        }
        @Override
        protected List doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                //  restTemplate.getForObject(url, User.class);
                ObjectMapper mapper = new ObjectMapper();
                List messagesList;
                if(messages!=null) {
                    final String url =  DataAPP.url + MESSAGES_C + MESSAGES_SEND_C;
                    messagesList = restTemplate.postForObject(url, messages, List.class);
                } else {
                    final String url =  DataAPP.url + MESSAGES_C + MESSAGES_CHECK_C;
                    messagesList = restTemplate.postForObject(url, DataAPP.user.getPlayer().getName(), List.class);
                }
                if(messagesList!=null&&messagesList.size()>0) {
                    List<Messages> res = mapper.convertValue(messagesList, new TypeReference<List<Messages>>() {
                    });
                    return res;
                }
                return null;

            } catch (Exception e) {
                Log.e("CharacteristicsActivity", e.getMessage(), e);
            }

            return null;
        }//http://10.0.2.2:8080/user/autch

        @Override
        protected void onPostExecute(List<Messages> messagesList) {
            System.out.println("tesy");
            // TextView greetingIdText = (TextView) findViewById(R.id.idTest);
            // greetingIdText.setText(user.getName());
            if(messagesList==null){
                // errLogin();
            } else {
                for(Messages m : messagesList){
                    DataAPP.getDataAPP().checkMessages(m.getToWhom()).add(m);
                }
            }
            //rh(user.getPlayer());

        }

    }*/
}
