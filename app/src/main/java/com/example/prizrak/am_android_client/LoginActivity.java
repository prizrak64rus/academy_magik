package com.example.prizrak.am_android_client;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.prizrak.am_android_client.utils.DBHelper;
import com.example.prizrak.am_android_client.utils.DataAPP;
import com.example.prizrak.am_android_client.utils.ThreadMessages;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import ru.vvd.am.server.entity.UserE;
import ru.vvd.am.server.utils.RestURL;

/**
 * Created by Prizrak on 10.01.2018.
 */

public class LoginActivity extends Activity implements RestURL {

    DBHelper dbHelper;
    private static final String PORT = ":8080";
    AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        initFields();
        initDialog();
        login(null);
    }

    private void initFields(){
        dbHelper = new DBHelper(this);

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DBHelper.TABLE_AUTCH, null, null, null, null, null, null);
        if (c.getCount() != 0 && c.moveToFirst()) {
            String login = c.getString(c.getColumnIndex(DBHelper.COLUMN_AUTCH_LOGIN));
            String password = c.getString(c.getColumnIndex(DBHelper.COLUMN_AUTCH_PASSWORD));
            String ip = c.getString(c.getColumnIndex(DBHelper.COLUMN_AUTCH_IP));

            TextView LHostText = (TextView) findViewById(R.id.LHostText);
            LHostText.setText(ip);
            TextView LLoginText = (TextView) findViewById(R.id.LLoginText);
            TextView LPasswordText = (TextView) findViewById(R.id.LPasswordText);
            LLoginText.setText(login);
            LPasswordText.setText(password);
        }
        dbHelper.close();
    }

    private void initDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(getLayoutInflater().inflate(R.layout.dialog_loading, null));
        builder.setCancelable(false);
        dialog = builder.create();
    }

    public void login(View v) {
        TextView LHostText = (TextView) findViewById(R.id.LHostText);
        DataAPP.url = LHostText.getText().toString()+PORT;
        DataAPP.url = "http://" + DataAPP.url;

        TextView LLoginText = (TextView) findViewById(R.id.LLoginText);
        TextView LPasswordText = (TextView) findViewById(R.id.LPasswordText);

        UserE user = new UserE();
        user.setName(LLoginText.getText().toString());
        user.setPassword(LPasswordText.getText().toString());

        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        cv.put(DBHelper.COLUMN_AUTCH_LOGIN, LLoginText.getText().toString());
        cv.put(DBHelper.COLUMN_AUTCH_PASSWORD, LPasswordText.getText().toString());
        cv.put(DBHelper.COLUMN_AUTCH_IP, LHostText.getText().toString());

        db.delete(DBHelper.TABLE_AUTCH, null, null);
        long rowID = db.insert(DBHelper.TABLE_AUTCH, null, cv);
        dbHelper.close();
        dialog.show();
        HttpRequestTask hrt = new HttpRequestTask();
        hrt.setValue(user);
        hrt.execute();

    }

    public void errLogin() {
        TextView LErrorText = (TextView) findViewById(R.id.LErrorText);
        LErrorText.setText("User or server not found");
        if(dialog.isShowing()){
            dialog.hide();
        }
    }

    public void okLogin() {
        DataAPP.threadMessages = (ThreadMessages) initThread(DataAPP.threadMessages, true);
        Intent SecAct = new Intent(getApplicationContext(), RootActivity.class);
        if(dialog.isShowing()){
            dialog.hide();
        }
        startActivity(SecAct);
        finish();
    }

    private Thread initThread(Thread thread, boolean isMess) {
        if (thread != null) {
            thread.stop();
        }
        thread = new ThreadMessages();
        thread.start();
        return thread;
    }

    private class HttpRequestTask extends AsyncTask<Void, Void, UserE> {
        private UserE user;

        public void setValue(UserE user) {
            this.user = user;
        }

        @Override
        protected UserE doInBackground(Void... params) {
            try {

                final String url = DataAPP.url + USER_C + USER_C_AUTCH;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                //  restTemplate.getForObject(url, User.class);
                user = restTemplate.postForObject(url, user, UserE.class);
                return user;
            } catch (Exception e) {
                Log.e("CharacteristicsActivity", e.getMessage(), e);
            }

            return null;
        }//http://10.0.2.2:8080/user/autch

        @Override
        protected void onPostExecute(UserE user) {
            System.out.println("tesy");
            // TextView greetingIdText = (TextView) findViewById(R.id.idTest);
            // greetingIdText.setText(user.getName());
            if (user == null) {
                errLogin();
            } else {
                DataAPP.user = user;
                okLogin();
            }
            //rh(user.getPlayer());

        }

    }
}
