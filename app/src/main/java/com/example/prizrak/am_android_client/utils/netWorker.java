package com.example.prizrak.am_android_client.utils;

/**
 * Created by Prizrak on 07.11.2017.
 */

public class netWorker {
    private netWorker(){

    }
    private static netWorker worker;
    public static netWorker getNetWorker(){
        if(worker==null){
            worker=new netWorker();
        }
        return worker;
    }
    private String ip="http://10.0.2.2:8080";

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
