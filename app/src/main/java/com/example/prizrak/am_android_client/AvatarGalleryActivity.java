package com.example.prizrak.am_android_client;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.prizrak.am_android_client.adapter.BaseAvatarAdapter;
import com.example.prizrak.am_android_client.adapter.BaseFolderAdapter;
import com.example.prizrak.am_android_client.subview.PreGenActivity;
import com.example.prizrak.am_android_client.utils.AvatarItem;
import com.example.prizrak.am_android_client.utils.DBHelper;
import com.example.prizrak.am_android_client.utils.DataAPP;
import com.example.prizrak.am_android_client.utils.FolderItem;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ru.vvd.am.server.utils.FileUtils;

/**
 * Created by Prizrak on 04.05.2018.
 */

public class AvatarGalleryActivity extends PreGenActivity implements IEditAvaterGallery {

    ArrayList<FolderItem> folderItems = new ArrayList<>();
    BaseFolderAdapter folderAdapter;
    RecyclerView folderRecyclerView;
    HashMap<FolderItem, List<AvatarItem>> itemListHashMap = new HashMap<>();

    RecyclerView avatarRecyclerView;
    BaseAvatarAdapter avatarAdapterBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_avatar_gallery);

        initFolder();
        initImageGalery();
    }

    private void initImageGalery() {
        avatarRecyclerView = (RecyclerView) findViewById(R.id.imagegallery);
        avatarRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 4);
        avatarRecyclerView.setLayoutManager(layoutManager);
        ArrayList<AvatarItem> avatarItems = prepareDataImageGalery();
        avatarAdapterBase = new BaseAvatarAdapter(getApplicationContext(), avatarItems, this, this);
        avatarRecyclerView.setAdapter(avatarAdapterBase);
    }

    private void initFolder() {
        folderRecyclerView = (RecyclerView) findViewById(R.id.folder);
        folderRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        folderRecyclerView.setLayoutManager(layoutManager);
        ArrayList<FolderItem> items = prepareDataFolder();
        folderAdapter = new BaseFolderAdapter(getApplicationContext(), items, this, this);
        folderRecyclerView.setAdapter(folderAdapter);
    }

    private ArrayList<FolderItem> prepareDataFolder() {
        dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.query(DBHelper.TABLE_GALLERY_FOLDER, null, null, null, null, null, "id");
        if (c.getCount() != 0 && c.moveToFirst())
            do {
                FolderItem item = new FolderItem();
                item.setFolder_name(c.getString(c.getColumnIndex(DBHelper.COLUMN_GALLERY_FOLDER_NAME)));
                item.setId(c.getInt(c.getColumnIndex(DBHelper.COLUMN_GALLERY_FOLDER_PRIORITY)));
                folderItems.add(item);
            } while (c.moveToNext());
        dbHelper.close();
        return folderItems;
    }

    private ArrayList<AvatarItem> prepareDataImageGalery() {

        ArrayList<AvatarItem> theimage = new ArrayList<>();
       /* AvatarItem avatarItem = new AvatarItem();
        avatarItem.setImage_title(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName());
        avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(DataAPP.getDataAPP().user.getPersonality().getPersonalityData().getFirstName()));
        theimage.add(avatarItem);

        avatarItem = new AvatarItem();
        avatarItem.setImage_title(FileUtils.TIME_OUT);
        avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(FileUtils.TIME_OUT));
        theimage.add(avatarItem);
        if (DataAPP.user.getName().equals("fux")) {
            avatarItem = new AvatarItem();
            avatarItem.setImage_title(DataAPP.FON_S);
            avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(DataAPP.FON_S));
            theimage.add(avatarItem);
        }
        for (String key : RootActivity.bitmapMap.keySet()) {
            if (DataAPP.user.getUserList().contains(key) || key.equals(FileUtils.TIME_OUT) || key.equals(DataAPP.FON_S))
                continue;
            avatarItem = new AvatarItem();
            avatarItem.setImage_title(key);
            avatarItem.setImage_Bitmap(RootActivity.bitmapMap.get(key));
            theimage.add(avatarItem);
        }*/

        {
            dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = db.query(DBHelper.TABLE_ITEM_GALLERY_FOLDER, null, null, null, null, null, DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER);
            int keyFolder = -1;
            List<AvatarItem> avatarItems = new ArrayList<>();
            if (c.getCount() != 0 && c.moveToFirst()) {
                if (keyFolder == -1) {
                    keyFolder = c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER));
                }
                do {
                    if (keyFolder != c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER))) {
                        for (FolderItem folderItem : folderItems) {
                            if (folderItem.getId() == keyFolder) {
                                itemListHashMap.put(folderItem, avatarItems);
                                break;
                            }
                        }
                        keyFolder = c.getInt(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ID_FOLDER));
                        avatarItems = new ArrayList<>();
                    }
                    AvatarItem item = new AvatarItem();
                    item.setImage_title(c.getString(c.getColumnIndex(DBHelper.COLUMN_ITEM_GALLERY_FOLDER_ART_NAME)));
                    item.setImage_Bitmap(RootActivity.bitmapMap.get(item.getImage_title()));
                    avatarItems.add(item);
                } while (c.moveToNext());
            }
            for (FolderItem folderItem : folderItems) {
                if (folderItem.getId() == keyFolder) {
                    itemListHashMap.put(folderItem, avatarItems);
                    break;
                }
            }
            dbHelper.close();
        }
        return theimage;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onSelectFolder(final FolderItem folderItem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                folderAction(true, folderItem);
            }
        });
    }

    protected void folderAction(boolean isSelect, FolderItem folderItem) {
        if (isSelect) {
            List<AvatarItem> avatarItems = itemListHashMap.get(folderItem);
            if (avatarItems == null) {
                avatarItems = new ArrayList<>();
                itemListHashMap.put(folderItem, avatarItems);
            }
            BaseAvatarAdapter avatarAdapter = new BaseAvatarAdapter(getApplicationContext(), avatarItems, this, this);
            setTitleActionBar(folderItem.getFolder_name());
            avatarRecyclerView.setAdapter(avatarAdapter);
        } else {
            setTitleActionBar("Академия магии...");
            avatarRecyclerView.setAdapter(avatarAdapterBase);
        }
    }

    @Override
    public void onAddAvatarFromFolder(FolderItem folderItem, String avatarName) {

    }

    @Override
    public void onDeleteFolder(FolderItem folderItem) {

    }

    @Override
    public void onDeleteAvatar(AvatarItem avatarItem) {

    }
}
