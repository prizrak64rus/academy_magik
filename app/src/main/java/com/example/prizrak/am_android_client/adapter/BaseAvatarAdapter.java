package com.example.prizrak.am_android_client.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.prizrak.am_android_client.utils.AvatarItem;
import com.example.prizrak.am_android_client.R;
import com.example.prizrak.am_android_client.RootActivity;
import com.example.prizrak.am_android_client.utils.IEditAvaterGallery;
import com.example.prizrak.am_android_client.utils.MyDragShadowBuilder;

import java.util.List;

/**
 * Created by Prizrak on 04.05.2018.
 */

public class BaseAvatarAdapter extends RecyclerView.Adapter<BaseAvatarAdapter.ViewHolder> {
    protected List<AvatarItem> galleryList;
    protected Context context;
    protected Activity activity;
    protected IEditAvaterGallery editAvaterGallery;

    public BaseAvatarAdapter(Context context, List<AvatarItem> galleryList, Activity activity, IEditAvaterGallery editAvaterGallery) {
        this.galleryList = galleryList;
        this.context = context;
        this.activity = activity;
        this.editAvaterGallery = editAvaterGallery;
    }

    @Override
    public BaseAvatarAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_avatar, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BaseAvatarAdapter.ViewHolder viewHolder, int i) {
        viewHolder.title.setText(galleryList.get(i).getImage_title());
        viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        viewHolder.img.setImageBitmap((galleryList.get(i).getImage_Bitmap()));
        viewHolder.img.setTag(galleryList.get(i).getImage_title());
        initClickAction(viewHolder, i);
    }

    protected void initClickAction(final ViewHolder viewHolder, int i){
        viewHolder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RootActivity.CHANGE = v.getTag().toString();
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView title;
        protected ImageView img;

        public ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.title);
            img = (ImageView) view.findViewById(R.id.img);
        }
    }


}